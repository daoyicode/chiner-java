package cn.com.chiner.java.antlr4.chain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/11/3
 * 实现功能 :
 */
public class AnnotationIgnoreProcess extends SqlProcess{

    @Override
    public String apply(String sql) {
        String regex = "/\\*(?s:(?!\\*/).)*\\*/";
        Pattern compile = Pattern.compile(regex);
        Matcher matcher = compile.matcher(sql);
        sql = matcher.replaceAll(" ");
        return process(sql);
    }
}
