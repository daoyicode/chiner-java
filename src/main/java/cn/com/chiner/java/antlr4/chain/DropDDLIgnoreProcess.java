package cn.com.chiner.java.antlr4.chain;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/10/19
 * 实现功能 :
 */
public class DropDDLIgnoreProcess extends SqlProcess {
    @Override
    public String apply(String sql) {
        String regex = "(?i)\\b^IF( )*EXIST.*"; //直接将IF开头的这种drop语句干掉
        Pattern compile = Pattern.compile(regex,Pattern.MULTILINE);
        Matcher matcher = compile.matcher(sql);
        sql = matcher.replaceAll(" ");
        return process(sql);
    }
}

