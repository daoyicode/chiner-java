package cn.com.chiner.java.antlr4.chain;

import cn.com.chiner.java.antlr4.sql.oceanbase.constant.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/10/19
 * 实现功能 :
 */
public class EmptyStringProcess extends SqlProcess {

    public static Logger logger = LoggerFactory.getLogger(EmptyStringProcess.class);
    @Override
    public String apply(String sql) {
        //antlr4对空字符串不友好，这里替换成常量，解析完之后替换回来
        sql = sql.replaceAll("\"\"", Constants.EMPTY_STRING);
        sql = sql.replaceAll("\'\'",Constants.EMPTY_STRING);
        return process(sql);
    }
}
