package cn.com.chiner.java.antlr4.chain;

import java.util.Objects;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/10/19
 * 实现功能 :
 */
public abstract class SqlProcess {
    protected SqlProcess next;

    public String apply(String sql) {
        return null;
    }

    public String process(String sql){
        if (Objects.nonNull(next)){
            return next.apply(sql);
        }
        return sql;
    }

    public SqlProcess getNext() {
        return next;
    }

    public void setNext(SqlProcess next) {
        this.next = next;
    }
}
