// Generated from C:/Users/24584/IdeaProjects/chiner-java/src/main/resources/g4/oceanbase/OBCreate.g4 by ANTLR 4.13.1
package cn.com.chiner.java.antlr4.sql.oceanbase.gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link OBCreateParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface OBCreateVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(OBCreateParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#databaseDDL}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabaseDDL(OBCreateParser.DatabaseDDLContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#databaseUse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabaseUse(OBCreateParser.DatabaseUseContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#ddl_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDdl_table(OBCreateParser.Ddl_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#ddl_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDdl_primary_key(OBCreateParser.Ddl_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#ddl_index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDdl_index(OBCreateParser.Ddl_indexContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#filter_index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilter_index(OBCreateParser.Filter_indexContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_definition_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_definition_list(OBCreateParser.Table_definition_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_definition(OBCreateParser.Table_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_definition_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_definition_list(OBCreateParser.Column_definition_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_definition(OBCreateParser.Column_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_option_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_option_list(OBCreateParser.Table_option_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_option}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_option(OBCreateParser.Table_optionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_name_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name_list(OBCreateParser.Column_name_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#comments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComments(OBCreateParser.CommentsContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#columnDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDesc(OBCreateParser.ColumnDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#autoIncrement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAutoIncrement(OBCreateParser.AutoIncrementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#notNull}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotNull(OBCreateParser.NotNullContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNull(OBCreateParser.NullContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#primaryKey}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKey(OBCreateParser.PrimaryKeyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#uniqueKey}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUniqueKey(OBCreateParser.UniqueKeyContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#character}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacter(OBCreateParser.CharacterContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#collate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollate(OBCreateParser.CollateContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#charset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharset(OBCreateParser.CharsetContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault(OBCreateParser.DefaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tableDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableDesc(OBCreateParser.TableDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#engine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEngine(OBCreateParser.EngineContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#rowFormat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRowFormat(OBCreateParser.RowFormatContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tableAutoIncrement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableAutoIncrement(OBCreateParser.TableAutoIncrementContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tableCharset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableCharset(OBCreateParser.TableCharsetContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tableCollate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableCollate(OBCreateParser.TableCollateContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment(OBCreateParser.CommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#comment_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment_value(OBCreateParser.Comment_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#data_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitData_type(OBCreateParser.Data_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tenant_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTenant_name(OBCreateParser.Tenant_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#pool_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPool_name(OBCreateParser.Pool_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#unit_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnit_name(OBCreateParser.Unit_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#zone_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZone_name(OBCreateParser.Zone_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#region_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegion_name(OBCreateParser.Region_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(OBCreateParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(OBCreateParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_alias_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias_name(OBCreateParser.Table_alias_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(OBCreateParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_alias_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias_name(OBCreateParser.Column_alias_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#partition_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartition_name(OBCreateParser.Partition_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#subpartition_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubpartition_name(OBCreateParser.Subpartition_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(OBCreateParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#view_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitView_name(OBCreateParser.View_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#object_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObject_name(OBCreateParser.Object_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#constraint_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint_name(OBCreateParser.Constraint_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tablegroup_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTablegroup_name(OBCreateParser.Tablegroup_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#outline_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOutline_name(OBCreateParser.Outline_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#user_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUser_name(OBCreateParser.User_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_factor(OBCreateParser.Table_factorContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#column_factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_factor(OBCreateParser.Column_factorContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(OBCreateParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#constant_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_value(OBCreateParser.Constant_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#operator_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator_expression(OBCreateParser.Operator_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#function_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_expression(OBCreateParser.Function_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#primary_zone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_zone(OBCreateParser.Primary_zoneContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#zone_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitZone_list(OBCreateParser.Zone_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#replica_num}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplica_num(OBCreateParser.Replica_numContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#tablegroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTablegroup(OBCreateParser.TablegroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#default_tablegroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_tablegroup(OBCreateParser.Default_tablegroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#table_tablegroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_tablegroup(OBCreateParser.Table_tablegroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#index_desc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_desc(OBCreateParser.Index_descContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#index_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_type(OBCreateParser.Index_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#charset_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharset_name(OBCreateParser.Charset_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#character_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacter_name(OBCreateParser.Character_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(OBCreateParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#logging_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogging_name(OBCreateParser.Logging_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServerCommonts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServerCommonts(OBCreateParser.SqlServerCommontsContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServerCommentDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServerCommentDesc(OBCreateParser.SqlServerCommentDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServerSchemaDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServerSchemaDesc(OBCreateParser.SqlServerSchemaDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServerTableDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServerTableDesc(OBCreateParser.SqlServerTableDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServerColumnDesc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServerColumnDesc(OBCreateParser.SqlServerColumnDescContext ctx);
	/**
	 * Visit a parse tree produced by {@link OBCreateParser#sqlServer_comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqlServer_comment(OBCreateParser.SqlServer_commentContext ctx);
}