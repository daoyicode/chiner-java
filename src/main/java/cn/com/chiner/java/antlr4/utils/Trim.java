package cn.com.chiner.java.antlr4.utils;


import org.apache.commons.lang3.StringUtils;

/**
 * 模块名 : Trim
 * 文件名 : Trim.java
 * 创建时间 : 2023/8/26
 * 实现功能 : 解决部分数据库连接工具导出时，字段名带有`or'的问题
 * 作者 : wantyx
 * 版本 : v0.0.1
 *
 * @see
 * @since ----------------------------------------------------------------
 * 修改记录
 * 日 期     版本     修改人  修改内容
 * 2023/8/26   v0.0.1    wantyx   创建
 * ----------------------------------------------------------------
 */
public class Trim {

    public static String trimName(String name){
        name = name.trim();
        String result = name;
        if (StringUtils.startsWithIgnoreCase(name,"`")
                && StringUtils.endsWithIgnoreCase(name,"`")){
            result = name.substring(1,name.length()-1);
        }
        if (StringUtils.startsWithIgnoreCase(name, "'")
                && StringUtils.endsWithIgnoreCase(name, "'")) {
            result = name.substring(1,name.length()-1);
        }
        return result;
    }
}
