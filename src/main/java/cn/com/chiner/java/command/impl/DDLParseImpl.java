package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.antlr4.listener.CustomListener;
import cn.com.chiner.java.antlr4.sql.oceanbase.OceanBaseVisitorImpl;
import cn.com.chiner.java.antlr4.sql.oceanbase.constant.Constants;
import cn.com.chiner.java.antlr4.sql.oceanbase.gen.OBCreateLexer;
import cn.com.chiner.java.antlr4.sql.oceanbase.gen.OBCreateParser;
import cn.com.chiner.java.antlr4.utils.SqlProcessor;
import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.hutool.core.collection.CollectionUtil;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class DDLParseImpl extends AbstractDBCommand<ExecResult>{
    @Override
    public ExecResult exec(Map<String, String> params) throws IOException {
        String filePath = params.get("ddlFile");
        InputStream inputStream = SqlProcessor.processFile(filePath);
        OBCreateLexer obCreateLexer = new OBCreateLexer(CharStreams.fromStream(inputStream));
        CommonTokenStream commonTokenStream = new CommonTokenStream(obCreateLexer);
        OBCreateParser obCreateParser = new OBCreateParser(commonTokenStream);
        obCreateParser.setBuildParseTree(true);
        obCreateParser.removeErrorListeners();
        obCreateParser.addErrorListener(new CustomListener());
        OBCreateParser.ProgContext prog = obCreateParser.prog();
        OceanBaseVisitorImpl obVisitor = new OceanBaseVisitorImpl();
        prog.accept(obVisitor);
        Map<String, TableEntity> tableMap = obVisitor.getTableMap();
        Collection<TableEntity> values = tableMap.values();
        //antlr4对空字符串不友好，空字符串预处理，解析完成之后替换回去，可见EmptyStringProcess类
        emptyStringProcess(values);
        //tableEntity加上行号 用于查看是否有字段在解析过程中遗漏
        fillRowNo(values);
        ExecResult execResult = new ExecResult();
        execResult.setStatus("success");
        execResult.setBody(values);
        return execResult;
    }

    /**
     * @param tableEntities 所有的表json结构
     */
    private void fillRowNo(Collection<TableEntity> tableEntities){
        if (CollectionUtil.isEmpty(tableEntities)){
            return;
        }
        AtomicInteger tableRowNo = new AtomicInteger();
        tableEntities.forEach(tableEntity -> {
            tableEntity.setRowNo(tableRowNo.getAndIncrement());
            List<ColumnField> fields = tableEntity.getFields();
            if (CollectionUtil.isNotEmpty(fields)){
                AtomicInteger columnRowNo = new AtomicInteger();
                fields.forEach(columnField -> columnField.setRowNo(columnRowNo.getAndIncrement()));
            }
        });
    }

    private void emptyStringProcess(Collection<?> entities){
        for (Object entity : entities) {
            if (entity instanceof TableEntity){
                TableEntity tableEntity = (TableEntity) entity;
                String comment = tableEntity.getComment();
                //如果是替换的空字符串标识，换回去
                if (Constants.EMPTY_STRING.equals(comment)){
                    tableEntity.setComment("");
                }
                List<ColumnField> fields = tableEntity.getFields();
                emptyStringProcess(fields);
            } else if (entity instanceof ColumnField) {
                ColumnField columnField = (ColumnField) entity;
                String comment = columnField.getComment();
                if (Constants.EMPTY_STRING.equals(comment)){
                    columnField.setComment("");
                }
                String defaultValue = columnField.getDefaultValue();
                if (Constants.EMPTY_STRING.equals(defaultValue)){
                    columnField.setDefaultValue("");
                }
            }
        }
    }

}
