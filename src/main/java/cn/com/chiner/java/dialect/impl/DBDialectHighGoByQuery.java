/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.dialect.impl;

import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.fisok.raw.kit.JdbcKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2024/1/24
 * @desc : 瀚高数据库解析
 */
public class DBDialectHighGoByQuery extends DBDialectMetaByQuery {
    private static Logger logger = LoggerFactory.getLogger(DBDialectHighGoByQuery.class);

    @Override
    protected String getQueryTablesSQL() {
        return "SELECT c.relname as tbl_name,\n" +
                "       cast(obj_description(c.relfilenode,'pg_class') as varchar) as tbl_comment,\n" +
                "       b.schemaname as db_name\n" +
                "FROM pg_class as c,\n" +
                "     pg_tables as b\n" +
                "WHERE\n" +
                "        c.relname = b.tablename\n" +
                "  AND b.schemaname=?";
    }

    @Override
    protected String getQueryTableColumnsSQL() {
        return "select\n" +
                "    col.table_name as tbl_name,\n" +
                "    '' as tbl_comment,\n" +
                "    col.ordinal_position as col_index,\n" +
                "    col.column_name as col_name,\n" +
                "    d.description as col_comment,\n" +
                "    col.udt_name as data_type ,\n" +
                "    col.character_maximum_length as data_length,\n" +
                "    col.numeric_scale as num_scale,\n" +
                "    tc.constraint_type as is_primary_key,\n" +
                "    col.is_nullable as is_nullable,\n" +
                "    col.column_default as default_value\n" +
                "from\n" +
                "    information_schema.columns col\n" +
                "        join pg_class c on c.relname = col.table_name\n" +
                "        left join pg_description d on d.objoid = c.oid and d.objsubid = col.ordinal_position\n" +
                "        left join information_schema.key_column_usage tku on tku.table_name = col.table_name and tku.column_name = col.column_name\n" +
                "        left join information_schema.table_constraints tc on tc.constraint_name = tku.constraint_name\n" +
                "where\n" +
                "        1 = 1\n" +
                "  and col.table_schema = ?\n" +
                "  and upper(col.table_name)=?\n" +
                "order by\n" +
                "    col.ordinal_position asc";
    }

    @Override
    public List<TableEntity> getAllTables(Connection conn, String schema) throws SQLException {
        if(schema == null){
            super.getAllTables(conn, conn.getSchema());
        }
        return super.getAllTables(conn, schema);
    }

    @Override
    public TableEntity createTableEntity(Connection conn, DatabaseMetaData meta, String tableName, String schema) throws SQLException {
        String sql = getQueryTableColumnsSQL();
        logger.debug(sql);

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, schema);
        pstmt.setString(2, tableName);


        TableEntity tableEntity = new TableEntity();
        tableEntity.setDefKey(tableName);

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            tableEntity.setDefKey(rs.getString("tbl_name"));
            tableEntity.setComment(rs.getString("tbl_comment"));
            ColumnField field = new ColumnField();
            tableEntity.getFields().add(field);
            field.setDefKey(rs.getString("col_name"));
            field.setDefName(rs.getString("col_comment"));

            String dataType = rs.getString("data_type");
            Integer dataLength = rs.getInt("data_length");
            Integer numScale = rs.getInt("num_scale");
            if(withoutLenDataTypeName(dataType))
            {
                dataLength = null;
                numScale = null;
            }
            String isNullable = rs.getString("is_nullable");//  Y|N
            String isPrimaryKey = rs.getString("is_primary_key");//
            String defaultValue = rs.getString("default_value");//

            //数据类型以及长度
            field.setType(dataType);
            if (numScale != null && numScale > 0) {
                field.setLen(dataLength);
                if (numScale != null && numScale > 0) {
                    field.setScale(numScale);
                }
            } else if (dataLength != null && dataLength > 0) {
                field.setLen(dataLength);
            }
            field.setNotNull("NO".equals(isNullable));
            field.setPrimaryKey("PRIMARY KEY".equals(isPrimaryKey));
//            if (dataType.toLowerCase().indexOf("char") >= 0) {
            if (defaultValue != null
                    && !defaultValue.startsWith("'")
                    && isStringDataType(dataType) ) {
                defaultValue = "'" + defaultValue + "'";
            }
            //处理：'M'::character varying 这种形式的默认值
            if(defaultValue != null && defaultValue.indexOf("::")>0){
                defaultValue = defaultValue.substring(0,defaultValue.indexOf("::"));
            }
            field.setDefaultValue(defaultValue);
        }

        JdbcKit.close(pstmt);
        JdbcKit.close(rs);

        return tableEntity;
    }
}
