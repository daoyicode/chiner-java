/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.dialect.impl;

import cn.com.chiner.java.model.TableIndex;
import cn.com.chiner.java.model.TableIndexColumnField;
import cn.fisok.raw.kit.JdbcKit;
import cn.fisok.raw.kit.StringKit;
import cn.com.chiner.java.command.kit.ConnParseKit;
import cn.com.chiner.java.dialect.DBDialect;
import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2021/6/14
 * @desc : SQLServer数据库方言
 */
public class DBDialectSQLServer extends DBDialectMetaByQuery {
    private static Logger logger = LoggerFactory.getLogger(DBDialectPostgreSQLByQuery.class);


    public List<TableEntity> getAllTables(Connection conn, String schema) throws SQLException {
        return super.getAllTables(conn,"");
    }


    protected String getQueryTablesSQL() {
        return "select top 10000\n" +
                "\tROW_NUMBER() OVER (ORDER BY t.table_name) AS tbl_order,\n" +
                "        t.table_name AS tbl_name,\n" +
                "       CONVERT(NVARCHAR(1000),isnull(c.[value],'')) AS tbl_comment,\n" +
                "    t.table_catalog as db_name\n" +
                "from INFORMATION_SCHEMA.tables t\n" +
                "         left join sys.extended_properties c\n" +
                "                   on OBJECT_ID(t.table_name)= c.major_id\n" +
                "                       AND c.minor_id = 0\n" +
                "                       AND class_desc = 'OBJECT_OR_COLUMN'\n" +
                "                       AND name = 'MS_Description'\n" +
                "where '1' <> ?" ;
    }

    protected String getQueryTableColumnsSQL() {
        return "SELECT\n" +
                "        tbl_name = d.name,\n" +
                "        tbl_comment = '',\n" +
                "        col_index = a.colorder,\n" +
                "        col_name = a.name,\n" +
                "        col_comment = CONVERT(nvarchar(1000),ISNULL(g.[value],'')),\n" +
                "        data_type = b.name,\n" +
                "        num_precision =\n" +
                "        CASE\n" +
                "            WHEN COLUMNPROPERTY(a.id, a.name, 'PRECISION' ) <= 0 THEN NULL ELSE COLUMNPROPERTY(a.id, a.name, 'PRECISION' )\n" +
                "            END,\n" +
                "        num_scale =\n" +
                "        CASE\n" +
                "            WHEN ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 ) <= 0 THEN NULL ELSE ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 )\n" +
                "            END,\n" +
                "        is_nullable =\n" +
                "        CASE\n" +
                "            WHEN a.isnullable = 1 THEN 'Y' ELSE ''\n" +
                "            END,\n" +
                "        is_primary_key = (\n" +
                "            SELECT\n" +
                "                'Y'\n" +
                "            FROM\n" +
                "                information_schema.table_constraints AS tc,\n" +
                "                information_schema.key_column_usage AS kcu\n" +
                "            WHERE tc.constraint_name = kcu.constraint_name\n" +
                "              AND tc.constraint_type = 'PRIMARY KEY'\n" +
                "              AND tc.table_name = d.name\n" +
                "              AND kcu.column_name=a.name\n" +
                "        ),\n" +
                "        default_value = ISNULL(e.text, '')\n" +
                "FROM\n" +
                "    syscolumns a\n" +
                "        INNER JOIN sysobjects d ON a.id= d.id AND d.xtype= 'U' AND d.name<> 'dtproperties'\n" +
                "        LEFT JOIN systypes b ON a.xusertype= b.xusertype\n" +
                "        LEFT JOIN syscomments e ON a.cdefault= e.id\n" +
                "        LEFT JOIN sys.extended_properties g ON a.id= g.major_id AND a.colid= g.minor_id\n" +
                "WHERE '1' <> ? AND upper(d.name)=upper(?)\n" +
                "ORDER BY\n" +
                "    a.id,\n" +
                "    a.colorder";
    }

    @Override
    public TableEntity createTableEntity(Connection conn, DatabaseMetaData meta, String tableName, String schema) throws SQLException {
        String sql = getQueryTableColumnsSQL();
        logger.debug(sql);

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, "");
        pstmt.setString(2, tableName);


        TableEntity tableEntity = new TableEntity();
        tableEntity.setDefKey(tableName);

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            tableEntity.setDefKey(rs.getString("tbl_name"));
            tableEntity.setComment(rs.getString("tbl_comment"));
            ColumnField field = new ColumnField();
            tableEntity.getFields().add(field);
            field.setDefKey(rs.getString("col_name"));
            field.setDefName(rs.getString("col_comment"));

            String dataType = rs.getString("data_type");
            Integer numScale = rs.getInt("num_scale");
            Integer numPrecision = rs.getInt("num_precision");
            Integer dataLength = numPrecision;
            if(dataType.toLowerCase().indexOf("clob")>=0
                    || dataType.toLowerCase().indexOf("blob") >=0
                    || dataType.toLowerCase().indexOf("int") >=0
            )
            {
                numPrecision = null;
                dataLength = null;
                numScale = null;
            }
            String isNullable = rs.getString("is_nullable");//  Y|N
            String isPrimaryKey = rs.getString("is_primary_key");//
            String defaultValue = rs.getString("default_value");//

            //数据类型以及长度
            field.setType(dataType);
            if (numPrecision != null && numPrecision > 0) {
                field.setLen(numPrecision);
                if (numScale != null && numScale > 0) {
                    field.setScale(numScale);
                }
            } else if (dataLength != null && dataLength > 0) {
                field.setLen(dataLength);
            }
            field.setNotNull("NO".equals(isNullable));
            field.setPrimaryKey("YES".equals(isPrimaryKey));
            if (dataType.toLowerCase().indexOf("char") >= 0) {
                defaultValue = "'" + defaultValue + "'";
            }
            field.setDefaultValue(defaultValue);
        }

        JdbcKit.close(pstmt);
        JdbcKit.close(rs);

        return tableEntity;
    }
}
