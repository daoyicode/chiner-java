package cn.com.chiner.java.model;

/**
 * @author 陈家宁
 * @date 2024/4/22
 * @desc 列表头设置
 */
public class ColumnHeader {

    private String refKey;//列值

    private String value;//列名

    private Boolean hideInGraph;//显示与关系图

    private Boolean freeze; //是否冻结

    private Boolean enable;//是否开启

    public String getRefKey() {
        return refKey;
    }

    public void setRefKey(String refKey) {
        this.refKey = refKey;
    }

    public Boolean getHideInGraph() {
        return hideInGraph;
    }

    public void setHideInGraph(Boolean hideInGraph) {
        this.hideInGraph = hideInGraph;
    }

    public Boolean getFreeze() {
        return freeze;
    }

    public void setFreeze(Boolean freeze) {
        this.freeze = freeze;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
