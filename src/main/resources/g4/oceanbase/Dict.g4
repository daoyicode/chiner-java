grammar Dict;
import Common;

value: (SE (NAME|INT|','|';'|'，'|'/'|'.'|'。'|'('|')'|'（'|'）'|'-'|'——'|'"'|'“'|'”'|':')+ SE) | INT | 'NULL'|'null';

prop:
    ('INSERT '|'insert ') ('INTO '|'into ') (tablegroup_name'.')? table_name
    '(' column_name (',' column_name)* ')'
    ('VALUES '|'values ')
    '(' value (',' value)* ');';