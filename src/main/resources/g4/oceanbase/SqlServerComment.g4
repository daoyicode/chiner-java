grammar SqlServerComment;
import Common;

sqlServerCommonts:
    ('EXEC'|'exec') 'sp_addextendedproperty' sqlServerCommentDesc  sqlServerSchemaDesc sqlServerTableDesc sqlServerColumnDesc ;

sqlServerCommentDesc:
    ('\'MS_Description\'' ',' sqlServer_comment ',') |
    ('@name' '=' 'N\'MS_Description\'' ',' '@value' '=' sqlServer_comment  ',')
    ;
sqlServerSchemaDesc:
    ('\'SCHEMA\''|'\'schema\'') ',' tablegroup_name ',' |
    '@level0type' '=' 'N\'Schema\'' ',' '@level0name' '=' tablegroup_name ','
    ;
sqlServerTableDesc:
    ('\'TABLE\''|'\'table\'') ',' table_name ','|
    '@level1type' '=' 'N\'Table\'' ','  '@level1name' '=' table_name ','
    ;
sqlServerColumnDesc:
    ('\'COLUMN\''|'\'column\''|'null'|'NULL'|'NULL_') ',' (column_name|'null'|'NULL') ';'|
    '@level2type' '=' 'N\'Column\'' ',' '@level2name' '=' (column_name|'null'|'NULL') ';'
    ;

sqlServer_comment: (SE (NAME|'，'|';'|'/'|'('|'（'|')'|'）')+ SE) | INT |'NULL'|'null';

