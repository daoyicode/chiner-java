package cn.com.chiner.dbtype;

import cn.com.chiner.java.Application;
import org.junit.Test;

public class DorisTest {
    @Test
    public void pingDriverLoadTest(){
        String[] args =  new String[]{
                "PingLoadDriverClass",                      //执行什么命令
                "driver_class_name=com.mysql.cj.jdbc.Driver",
                "url=jdbc:mysql://g5.mtain.top:36007/demo?useUnicode=true&characterEncoding=UTF-8&useSSL=false",
                "username=root",
                "password=7mcz66qQ7UHD",
                "out=/Users/yangsong158/workspace/ws-chiner/chiner-java/src/test/resources/out/pdc-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }

    @Test
    public void listTableTest(){
        String[] args =  new String[]{
                "DBReverseGetAllTablesList",            //执行什么命令
                "driver_class_name=com.mysql.cj.jdbc.Driver",
                "url=jdbc:mysql://g5.mtain.top:36007/demo?useUnicode=true&characterEncoding=UTF-8&useSSL=false",
                "username=root",
                "password=7mcz66qQ7UHD",
                "out=/Users/yangsong158/workspace/ws-chiner/chiner-java/src/test/resources/out/dbrgatl-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }


    @Test
    public void getTableDDLTest(){
        String[] args =  new String[]{
                "DBReverseGetTableDDL",            //执行什么命令
                "driver_class_name=com.mysql.cj.jdbc.Driver",
                "url=jdbc:mysql://g5.mtain.top:36007/demo?useUnicode=true&characterEncoding=UTF-8&useSSL=false",
                "username=root",
                "password=7mcz66qQ7UHD",
                "tables=sims_class,sims_student",
                "out=/Users/asher/workspace/ws-vekai/chiner-java/src/test/resources/out/dbrgtddl-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }
}
