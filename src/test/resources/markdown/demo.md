**[PDManer元数建模-4.0]**，历时四年，持续升级，工匠精神，做一款简单好用的数据库建模平台。
> 元数建模平台，使用React+Electron+Java技术体系构建。

[PDMan-v2] --> [CHINER-v3] --> [PDManer-v4]，连续四年，产品一直保持很好的传承和延续。

# 1. PDManer的故事
1. 2018年初，因PowerDesigner对我们需求不能满足，也因为购买商业授权的不便利，我和几位社区好友，创立了一个松散的组织，用一个半月时时间完成了PDMan的1.0版本发布，解决了从无到有的问题。
2. 2018年5月，推出了PDMan第一个开源公开版，中间持续阶段性更新，直到2019年1月，不再更新。
3. 因前第一个版本时间仓促，设计考虑不充分，后续优化升级非常困难，我们于2019年12月，规划了另一个全新的版本。
4.  2019年底，我们不到三万块启动资金，启动创业，生存之难可以想像，当情怀遇上生存发展，饿着肚讲理想，真的很难。期间，我们团队几经折腾周转，数次濒临解散。幸得有好友关照，找了两个项目做，核心人员一分为二，一部分去杭州，另一部分在远走塞北，我们的吃饭问题暂时解决了。
7. 在此期间，产品停止更新，但是对产品的思考一直在持续，同时也结识了更多志趣相投的朋友加入，我们利用业余时间，完成了技术架构设计，界面原型设计，以及关键核心模块的开发编码。
8. 2021年7月17日，终于推出全新的3.0版本，为区别于PDMan2版本，因为一些特殊的原因，不得已而使用新名称，我们使用了[CHINER元数建模]作为新产品的名称，公开发行。
9. 目前，每一天，有50000家以上的组织或个人在使用CHINER在设计他们的数据库。
10. CHINER拼写虽然看上去比较协调，但是发音存在二义性，用户接受度不高，很多用户还是习惯叫他PDMan，在构建4.0版本时，我们想延续用户对PDMan的习惯，同时，也希望能够保留CHINER的某些记忆，英文名PDManer=PDMan+er(chiner的er部分，ER也表示关系图的意思)，“元数建模”的中文名称依然延续，名称需要精简，拿掉chi表示中国的前缀部分，使用中文能更加明确这是一个中国小团队的作品，因此4.0版本之后，产品名称：[PDManer元数建模]就此确定，承接了PDMan以及CHINER的所有功能，并且进行延续、精进。
11. 2022年4月17日，我们发布“PDManer元数建模-v4.0”版本，相对于3版本，我们增加了可定制的版本管理以及可定制的代码生成器（可生成Java，C#，等相关程序代码），一直被用户吐槽的mac版本下图标过大的问题，也一并解决了。
12. PDManer产品，主要针对单机用户，我们会持续保持他的开源免费。面向团队的版本，后续会逐步推出。

# 2. 新版本4.0增加的功能
1. 数据表版本管理，添加、更改、删除字段以及表信息后，会自动生成变更脚本。
2. 代码生成器，目前支持Mybatis,MybatisPlus,C#,JavaJPA等几个常见语言，在完成表设计之后，可以自动生成增删改查相关代码，并且可生成至相关目录
3. 批量修改数据表名功能
4. 用户可以自行添加或者编辑新的数据库模板，添加新的代码生成器
5. MacOS下，修正了图标过大的问题
6. 增加Hive的DDL生成的参考模板
7. 批量调整及以修改数据表名

# 3. 新版本4.0增加功能截图
## 3.1 版本管理
### 3.1.1 版本变更记录以及DDL脚本
![](https://oscimg.oschina.net/oscnet/up-c98812c43b468acd2846083efe8139c547e.png)
### 3.1.2 版本变更脚本定制
![](https://oscimg.oschina.net/oscnet/up-365ab3171f3e34131a8fc9595de059c3425.png)

## 3.2 代码生成
### 3.2.1 代码模板
![](https://oscimg.oschina.net/oscnet/up-66e449a2d58da1525d04f03610e8e25f534.png)
### 3.2.2 生成预览
![](https://oscimg.oschina.net/oscnet/up-64e8bdf6684145e99a7c206132b07035938.png)
### 3.2.3 生成至目录
![](https://oscimg.oschina.net/oscnet/up-d7aeecad99aa087be520e8e14f2aca3e1d5.png)

# 4. 功能介绍
**预览截图**  
![](https://oscimg.oschina.net/oscnet/up-c66bfac74997523d9107a7e38fcfd1a7b6a.png)  
![](https://oscimg.oschina.net/oscnet/up-ddfa1d72c9dc0049d1498b9bb1185ff5255.png)  
![](https://oscimg.oschina.net/oscnet/up-e363eb37bad6fa19340f46b4fed7f916983.png)


## 4.1 入门参考案例以及文档
### 4.1.1 操作手册
![](https://oscimg.oschina.net/oscnet/up-a0826c20c0c1e96f4a4afa72ed4aeffb202.png)  
手册地址：https://www.yuque.com/pdmaner/docs/pdmaner-manual
![](https://oscimg.oschina.net/oscnet/up-45d3d74df4f0dec2de713115ef1770097fd.png)


### 4.1.2 入门参考案例
首页自带两个典型参考案例，方便用户快速了解软件支持的功能以及特性。  
![](https://oscimg.oschina.net/oscnet/up-f7155f2810a712023034941d0e4a94337ad.png)

## 4.2 管理对象
### 4.2.1 数据表及字段
提供简洁直观的数据表以及字段管理及操作，左侧列表支持拖动排序，数据表更多设置支持增加表备注，扩展属性列表，例如提供对Hive的支持，如下图：   
![](https://oscimg.oschina.net/oscnet/up-30295867c1023ed392ccec546cbc3c06b39.png)

### 4.2.2 多表关联的视图
视图由多个表结合而成，支持多表以及字段的选择，如下图：  
![](https://oscimg.oschina.net/oscnet/up-e0f217c8a895ebb62d5cffad1a06f49c7f8.png)

### 4.2.3 可定制的数据类型及数据域
可扩展的数据类型，并且支持多种数据库方言的适配，如下图：  
![](https://oscimg.oschina.net/oscnet/up-d3057114daaebbf61c83d7cebe4ab2a6f58.png)

数据域，用于设置同一类具有特定业务含义的数据类型，如下图：  
![](https://oscimg.oschina.net/oscnet/up-caf42f4c46226bbf2111ab19766e44a7f35.png)  
![](https://oscimg.oschina.net/oscnet/up-d7c82607c6fe53eae01ab3f97984f52f60e.png)
### 4.2.4 数据标准（字段库）
标准字段库用于解决常用字段记录，方便用户建立数据表时，能够从常用字段库里直接拖入数据表中。
标准字段库可以用户自行添加，也可以从现有数据表中移到标准字段库中，如下图所示：  
![](https://oscimg.oschina.net/oscnet/up-2a410362c479627576f840825e0b27e4d30.png)  
![](https://oscimg.oschina.net/oscnet/up-9386768a01a28254415ee7ce1995a0e17c9.png)

标准字段库支持导出JSON文件，也支持从JSON文件中导入，以解决共享交流问题。
### 4.2.5 数据字典（代码映射表）
增加了数据字典支持，用于解决对字段元数据更清晰的解析阐述，如下图：  
![](https://oscimg.oschina.net/oscnet/up-41a9f54625194c5690e04ec89c91426f88e.png)

数据表字段可以直接关联数据字典，如下图所示：  
![](https://oscimg.oschina.net/oscnet/up-3668f16ef4a8fd4c012c6e1ccbab2f62048.png)

数据字典导出SQL，如下图所示：  
![](https://oscimg.oschina.net/oscnet/up-38158c2ded0d89a38b65a74413c6c73aa57.png)  
![](https://oscimg.oschina.net/oscnet/up-87a93e9b6e99139c3c2d386efec29f3f502.png)
## 4.3 多模块模式以及不分模块模式
简单项目，不需要分模块，直接分为数据表，视图，关系图，数据字典即可，复杂项目需要折分为一个一个独立的模块，系统对这两种形式均给予支持。
简单模式，如下图：  
![](https://oscimg.oschina.net/oscnet/up-6fe3809f46930d19eb26f81aeeb2a3d2042.png)

分模块形式，如下图：  
![](https://oscimg.oschina.net/oscnet/up-4b777fb0d36dd16163b7c004ed273b73c7f.png)

## 4.4 关系图
### 4.4.1 ER关联关系图
数据实体关联关系图，该关联关系图需要人工手动维护，如下图所示：  
![](https://oscimg.oschina.net/oscnet/up-50460efb7d2be4259c22c97395800e740b3.png)

### 4.4.2 简单的概念模型图
支持简单的概念模型图，概念模型图实体只保存在关系图上，不保持实体对象，如下图所示：
![](https://oscimg.oschina.net/oscnet/up-52bbc1755f4e24a126794bb7e2dd30aeab4.png)
![](https://oscimg.oschina.net/oscnet/up-e4cfa15a5cbd4f6d7b29776bc6c43fdbb32.png)

概念模型图，主要用于快速勾勒系统的关键业务对象关系图，用于快速整体理解数据模型。
### 4.4.3 同一模块多张关系图
同一个模块，可以支持多张多种形式的关系图：  
![](https://oscimg.oschina.net/oscnet/up-9f88becf7efa6aef3905ea9be7a8a123e28.png)

## 4.5 画布设计界面
### 4.5.1 分组框及以备注框
分组框，用于对数据表或者实体进行分类，能够更清晰的了解数据表的层次结构，如下图：  
![](https://oscimg.oschina.net/oscnet/up-5e6ac131fdc2af2992ed3a48fdad3386b2c.png)

### 4.5.2 文字以及背景颜色设置
备注框，为普通矩形框，用于对数据表或者业务场景进行解释说明，如下图：  
![](https://oscimg.oschina.net/oscnet/up-b71dccee9f4289216c4a7ec04a8053f8a6f.png)

## 4.6 代码模板
### 4.6.1 不同数据库方言的DDL
通过代码模板引擎，实现可扩展的数据库方言支持，如下图：

**MySQL：**
![](https://oscimg.oschina.net/oscnet/up-4781d66c4514866aeb81baf7d2d895fca44.png)  
**PostgreSQL:**  
![](https://oscimg.oschina.net/oscnet/up-c1d69f9ffd7fd52ba6321dee1e8ff089597.png)  
**ORACLE：**  
![](https://oscimg.oschina.net/oscnet/up-51df3064ae580e7236108e34d6f783e33b1.png)  
**SQLServer:**  
![](https://oscimg.oschina.net/oscnet/up-129f5c5ae0960660dabe47e3231d8dd043b.png)

### 4.6.2 扩展属性支持类似Hive
![](https://oscimg.oschina.net/oscnet/up-ecdae24a8c050395fcf1f45285a684dd912.png)

### 4.6.3 JavaBean代码生成
![](https://oscimg.oschina.net/oscnet/up-809118324fbb7baa16897f9bf868e30471d.png)

### 4.6.4 可定制化可编辑的代码模板引擎
代码模板引擎基于doT.js构建，如下图：  
![](https://oscimg.oschina.net/oscnet/up-b93a82f0511260cf8a055ed6beab008536f.png)  
提供代码预览编辑，以便能够及时预览代码模板的效果，如下图：  
![](https://oscimg.oschina.net/oscnet/up-931291d1ef72df39061b93b6885c56777c4.png)

## 4.7 生态对接-导入
### 4.7.1 数据库逆向
连接数据库，逆向解析数据库，支持数据库中文注释的解析。
连接数据库，如下图：

![](https://oscimg.oschina.net/oscnet/up-71491d4c34977ce3d346c0c8c9e7db3a765.png)

解析数据列表清单，如下图：

![](https://oscimg.oschina.net/oscnet/up-7c7f8e11a1bf441a020fc672db8b4fefac3.png)

解析数据表结果，如下图：

![](https://oscimg.oschina.net/oscnet/up-65a71a361a7736ba1eca69966e18ef5b40e.png)
### 4.7.2 导入PDMan文件
支持PDMan的导入，支持数据表，关系图，数据域的高度还原。
导入列表选择，如下图：

![](https://oscimg.oschina.net/oscnet/up-75a57b46446978aa720573b81fec227be77.png)

导入后结果，如下图：

![](https://oscimg.oschina.net/oscnet/up-a61507effd971c8c774f99da0a259619c64.png)

PDMan原始情况，如下图：

![](https://oscimg.oschina.net/oscnet/up-4ff384b4dabf15877ff50b7457b53f06487.png)
### 4.7.3 导入PowerDesigner文件
导入PowerDesigner，支持数据表，数据域的高度还原（不支持关系图还原），如下图：

![](https://oscimg.oschina.net/oscnet/up-8a10182e4059e2a6102f66a2b3cd7522ef2.png)

数据表选择，如下图：

![](https://oscimg.oschina.net/oscnet/up-dd1043e28054b37711139b679016ec77a9d.png)

最终导入后结果，如下图：

![](https://oscimg.oschina.net/oscnet/up-58c53a6e3ee6a22d429dd91a3c5fc59a328.png)

## 4.8 生态对接-导出
### 4.8.1 导出DDL
导出DDL，用于解决一次性导出指定数据表，针对指定数据库方言的导出，如下图：

![](https://oscimg.oschina.net/oscnet/up-22048d17e7b5b9b73742a382116eef0a25c.png)

### 4.8.2 导出WORD文档及模板可定制
将当前数据表，关系图，数据字典导出至WORD文档，如下图：

![](https://oscimg.oschina.net/oscnet/up-28dbbe80c7054ed5a28c05727e8a95ccec3.png)

导出结果，如下图：  
![](https://oscimg.oschina.net/oscnet/up-ad6471a8fd81aa67a9552f79c33aae322cb.png)  
![](https://oscimg.oschina.net/oscnet/up-d01a17aaa75ee5109e3406df9572de01a21.png)
### 4.8.3 关系图导出图片
实现将当前画布的关系图，导出为PNG图片。

## 5. 全局搜索及定位
字段及关键字，数据字典等的全局搜索，如下图：  
![](https://oscimg.oschina.net/oscnet/up-9af149ef54054b35266716f55b2dbff97f3.png)

## 6. 更多特性
### 6.1 多语言
中文，如下图：  
![](https://oscimg.oschina.net/oscnet/up-1a1888c779a8efcc18e36e62d10ce34762f.png)

英文，如下图：  
![](https://oscimg.oschina.net/oscnet/up-93ba0908b9dd24ceeb4f16e9e703d1114ad.png)

语言设置，如下图：  
![](https://oscimg.oschina.net/oscnet/up-6722ee98f359b9ce053a25907b9e52454cd.png)
### 6.2 新建表默认初始化字段
![](https://oscimg.oschina.net/oscnet/up-d87cc8da3669a23bfb7524629e77fd331b4.png)
### 6.3 表编辑一次性设置多个数据域
![](https://oscimg.oschina.net/oscnet/up-18b1f17e98a88199d473704d6d573afa867.png)
### 6.4 批量修改表名
![](https://oscimg.oschina.net/oscnet/up-4cca1bb24fa8b5cbecc7359634acef96c59.png)  
![](https://oscimg.oschina.net/oscnet/up-dc886c836a9781e6aa2bcdd54eb0d25a53a.png)
### 6.5 支持国产数据库，如达梦
![](https://oscimg.oschina.net/oscnet/up-1378d8ebecc7932d77b7630e6d4d9c6b26c.png)  
![](https://oscimg.oschina.net/oscnet/up-30ab1ab20793c0cbee8429645e25eab240d.png)

# 7. 开源协议说明
元数建模，采用[木兰公共许可证, 第2版](https://license.coscl.org.cn/MulanPubL-2.0/ "")开源协议，请按协议约定使用本产品。

# 8. 对社区用户的承诺
## 8.1 历史承诺盘点：
2018年3月，在苏州源创会，给用户承诺后续将完成以下功能：
![](https://oscimg.oschina.net/oscnet/up-84611232cb487501d86cc5bc3814b9f4aba.png)

- 1-提升用户体验
-  &nbsp;&nbsp;1-1 提升界面美观[完成]
-  &nbsp;&nbsp;1-2 优化用户操作[完成]
- 2-更多模型支持
- &nbsp;&nbsp;2-1 导入ERWin[未完成，视用户需求，再作决定]
- &nbsp;&nbsp;2-2 导入PowerDesigner[完成]
- 3-小型专业化社区[部分完成]

## 8.2 未来承诺
1. PDManer元数建模，作为一款国产免费开源数据库建模工具，源代码以及编译后的程序，都免费提供给个人或者组织使用。
2. 在此基础上二次开发，再次出售，需遵守[木兰公共许可证, 第2版](https://license.coscl.org.cn/MulanPubL-2.0/ "")。
3. 为简化用户使用，PDManer提供编译后的安装包。

# 9. 产品线（规划中）
为更好鼓励开源，激励开源贡献者的热情，生态良性发展，后续我们将尝试推出以下两个商业版：
- **云服务Web在线版**: 除保留原来的所有功能外，增加团队协作，版本管理，行业词库，智能纠正，WebHooks等团队及企业级应用功能。
- **企业私有部署Web版**：功能与Web在线版相同，提供企业私有化部署，满足企业数据代码资产要求内网部署的要求。

# 10. 下载及交流
## 10.1 源代码地址
前端JS：    [查看](https://gitee.com/robergroup/pdmaner.git "查看")
&nbsp;| &nbsp;后端Java：[查看](https://gitee.com/robergroup/chiner-java.git "查看")

## 10.2 下载及交流
同发知乎: [知乎](https://zhuanlan.zhihu.com/p/499897001)
安装文件下载及交流 [前往下载以及交流](https://gitee.com/robergroup/pdmaner/releases "前往下载以及交流地址")