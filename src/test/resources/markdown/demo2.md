> 以下操作都在案例模块下完成，代码路径：com.vekai.democ.smartoc.template.DemoPersonalInformationTemplate

### 1.文档配置

> 通过@TocTemplate注解将格式化模板渲染类（java类）注册到文档模板库中

``` java
@TocTemplate(name = "个人信息演示案例", group = "演示案例")
public class DemoPersonalInformationTemplate {
}
```

**@TocTemplate参数：**

1. name：模板名称
2. group：模板的分组，可通过“.”点来设置多级分组，例：group = 授信报告.个人报告

### 2.初始化参数 

> 用于初始化模板章节方法所需要的参数值，例子：多个章节方法中都需要客户信息的时候，可以将客户查询放在初始化参数方法中，再将客户信息作为参数传递，这样就可以在章节方法参数中接收到客户的信息。

``` java
@TocTemplate(name = "个人信息演示案例", group = "演示案例")
public class DemoPersonalInformationTemplate {
    
    /**
     * 初始化参数
     *
     * @param params   参数列表，必须存在该参数
     * @param personId 该参数用于接收业务编号，名称可自定义
     */
    @TocParams
    public void tocParams(MapObject params, Long personId) {
        // 获取个人信息
        DemoPerson demoPerson = beanCruder.selectOneById(DemoPerson.class, personId);
        ValidateKit.notNull(demoPerson, "该个人信息[{0}]不存在！", personId);
        params.put("demoPerson", demoPerson);
    }
}
```

说明：初始化方法需要加@ParamInit注解

### 3.章节配置

#### 3.1.word文档模板

> word文档必须为docx格式的，存储在项目的资源文件目录下

**文档截图：**

![image-20201127145403145](F:\工作\FISOK文档MD\FISOK开发手册\assets\image-20201127145403145.png)

说明：{{变量名}}需要填充数据的标签，[[变量名]]可编辑区域

#### 3.2.数据模型

``` java
@TocEntity("基本信息")
public class PersonalBasicData {
    
    @TocField("文档标题")
    private String docTitle;
    
    @TocField("性别")
    @TocDict("Gender")
    private String gender ;
    
    @TocFieldBookmark("爱好")
    private String hobby ;
}
```

**@TocEntity**：数据模型类注解

**@TocField**：填充字段

	1. labelKey：填充变量名（建议用中文）
 	2. renderType：渲染类型：None, Text, Table, Image, ImageList, ObjectNode, ObjectListNode
 	3. defaultValue：默认值
 	4. format：数据显示格式：###.00
 	5. formatSuffix：后缀：元

**@TocDict**：填充字段所需要的字典值

1. mode：字典模式：DictKey（数据字典），Manual（自定义字串）
2. dictKey：字典名称

**@TocFieldBookmark**：可编辑字段

1. labelKey：变量名称（建议用中文）
2. placeholder：输入区域提示内容

#### 3.3.渲染方法
> 通过@TocTemplateSection注解将格式化模板渲染类中的方法作为一个文档章节，返回一个TocSection章节对象。
>
> 步骤：将数据写入到数据模型对象中，然后通过数据模型对象将数据填充到word文档中。

``` java
@TocTemplate(name = "个人信息演示案例", group = "演示案例")
public class DemoPersonalInformationTemplate {
    
	@TocTemplateSection(name = "基本信息", order = 10, path = DOC_PATH + "个人信息演示案例-基本信息.docx")
    public TocSection basic(InputStream tplIn,
                            @RequestParam("demoPerson") DemoPerson demoPerson,
                            @RequestParam("docTitle") String docTitle) {
        // 拷贝个人信息到数据模型
        PersonalBasicData personalBasicData = TocSectionKit.castTo(demoPerson, PersonalBasicData.class);
        // 拷贝联系信息到数据模型
        PersonalContactData personalContactData = TocSectionKit.castTo(demoPerson, PersonalContactData.class);
        personalBasicData.setPersonalContactData(personalContactData);
        // 设置文档标题，该文档标题配置在[配置格式化文档-个人信息演示案例]文档参数中
        personalBasicData.setDocTitle(docTitle);

        // 返回TocSection对象
        return TocSectionKit.build(tplIn, personalBasicData);
    }
}
```

**说明：**参数1：InputStream tplIn是固定写法，用于接收配置的word文档输入流对象

**@TocTemplateSection：**

1. name：章节名称
2. order：排序码，文档模板库列表中的排序
3. path：文档存储路径
4. variables：该章节使用到的变量，一般为数据模型类

**@RequestParam**：用于接收**前端**传递的参数和**初始化参数方法**设置的参数，用法与springmvc注解一致。

**TocSectionKit工具介绍：**

| 方法                                             | 说明                                             |
| :----------------------------------------------- | ------------------------------------------------ |
| TocSectionKit.build(tplIn)                       | 如果不需要做数据填充，可以直接将文档作为章节对象 |
| TocSectionKit.build(tplIn, 数据模型对象)         | 将数据模型中的值填充到文档中                     |
| TocSectionKit.listBuild(tplIn, 数据模型列表对象) | 将返回章节列表                                   |

### 4.章节片段设置（投保人循环）

> 返回一个List<TocSection>对象

``` java
@TocTemplate(name = "个人信息演示案例", group = "演示案例")
public class DemoPersonalInformationTemplate {	
    
	@TocTemplateSection(name = "银行卡列表", order = 20, path = DOC_PATH + "个人信息演示案例-银行卡.docx")
    public List<TocSection> bankCard(InputStream tplIn, @RequestParam("demoPerson") DemoPerson demoPerson) {
        MapObject bankCardDatas = new MapObject();
        Map<String, Long> bankCardKeys = new HashMap<>();
        // 获取列表
        List<PersonalBankCardData> bankCards = demoPersonBankCardTemplateMapper.listBankCardByPersonId(demoPerson.getId());
        for (PersonalBankCardData bankCard : bankCards) {
            String issueBankName = bankCard.getIssueBankName();
            if (issueBankName != null) {
                bankCardDatas.put(issueBankName, bankCard);
                bankCardKeys.put(issueBankName, bankCard.getUid());
            }
        }

        // 返回TocSection对象
        return TocSectionKit.listBuild(tplIn, bankCardDatas, bankCardKeys);
    }
}
```

**代码变量说明：**

1. bankCardDatas：数据模型对象列表，key值为章节名称
2. bankCardKeys：数据模型对象对应的唯一流水号，key值为章节名称，如果不传入该对象，则无法做到输入区域数据的保留。
3. issueBankName：章节名称，显示文档左侧章节列表显示