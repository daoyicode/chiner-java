IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[demo_person_company]') AND type IN ('U'))
DROP TABLE [dbo].[demo_person_company]
    GO

CREATE TABLE [dbo].[demo_person_company] (
    [id] int  NOT NULL,
    [first_name] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [last_name] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [gender] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [date_of_birth] date  NULL,
    [place_of_birth] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [nationality] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [ethnicity] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [religion] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [mother_tongue] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [height] real  NULL,
    [weight] real  NULL,
    [eye_color] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [hair_color] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [skin_color] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [blood_type] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [home_address] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [postal_code] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [phone_number] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [email] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [social_security_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [passport_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [passport_expiration_date] date  NULL,
    [drivers_license_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [drivers_license_expiration_date] date  NULL,
    [bank_account_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [bank_name] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [bank_routing_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [credit_card_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [credit_card_expiration_date] date  NULL,
    [credit_card_cvv] nvarchar(10) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [emergency_contact_name] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [emergency_contact_phone] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [medical_conditions] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [allergies] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [current_medications] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [medical_insurance_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [medical_insurance_company] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [educational_level] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [major_field_of_study] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [degree_awarding_institution] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [graduation_date] date  NULL,
    [employment_status] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [employer_name] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [occupation] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [industry] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [work_phone_number] nvarchar(20) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [work_email] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [work_postal_address] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [monthly_income] real  NULL,
    [marital_status] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [spouse_name] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [spouse_occupation] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [children_names] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [children_birthdates] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [children_genders] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [parents_names] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [parents_occupations] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [siblings_names] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [siblings_birthdates] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [siblings_occupations] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [hobbies] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_music] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_movies] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_books] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [political_affiliation] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [computer_skills] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [languages_spoken] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [life_goals] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [biggest_achievement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [biggest_failure] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [biggest_regret] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [best_memory] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [personal_motto] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_quote] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [most_admired_person] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [personal_website] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [social_media_profiles] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [recent_travel_destinations] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [dream_travel_destination] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_cuisine] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_drink] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [pet_name] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [pet_type] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_sport] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_team] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_player] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [last_book_read] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_artist] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_album] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [favorite_song] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [musical_instrument] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [charity_work] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [volunteer_experience] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [latest_achievement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [biggest_challenge] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [future_plans] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [company_name] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NOT NULL,
    [company_code] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [registration_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [incorporation_date] date  NULL,
    [employee_count] int  NULL,
    [average_salary] decimal(10,2)  NULL,
    [main_business] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [industry_category] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [company_address] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [website_url] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [customer_service_phone] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [fax_number] nvarchar(50) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [email_address] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [legal_representative] nvarchar(100) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [registered_address] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [business_scope] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [registered_capital] decimal(10,2)  NULL,
    [annual_revenue] decimal(15,2)  NULL,
    [net_profit] decimal(15,2)  NULL,
    [total_assets] decimal(15,2)  NULL,
    [total_liabilities] decimal(15,2)  NULL,
    [shareholders_equity] decimal(15,2)  NULL,
    [retained_earnings] decimal(15,2)  NULL,
    [current_assets] decimal(15,2)  NULL,
    [fixed_assets] decimal(15,2)  NULL,
    [intangible_assets] decimal(15,2)  NULL,
    [long_term_debt] decimal(15,2)  NULL,
    [current_debt] decimal(15,2)  NULL,
    [cash_and_equivalents] decimal(15,2)  NULL,
    [accounts_receivable] decimal(15,2)  NULL,
    [inventory_value] decimal(15,2)  NULL,
    [prepaid_expenses] decimal(15,2)  NULL,
    [other_current_assets] decimal(15,2)  NULL,
    [property_plant_equipment] decimal(15,2)  NULL,
    [goodwill] decimal(15,2)  NULL,
    [patents_and_licenses] decimal(15,2)  NULL,
    [software_development_costs] decimal(15,2)  NULL,
    [deferred_tax_assets] decimal(15,2)  NULL,
    [other_non_current_assets] decimal(15,2)  NULL,
    [total_revenue] decimal(15,2)  NULL,
    [cost_of_goods_sold] decimal(15,2)  NULL,
    [gross_profit] decimal(15,2)  NULL,
    [operating_expenses] decimal(15,2)  NULL,
    [selling_expenses] decimal(15,2)  NULL,
    [general_administrative_expenses] decimal(15,2)  NULL,
    [research_development_expenses] decimal(15,2)  NULL,
    [operating_income] decimal(15,2)  NULL,
    [interest_expense] decimal(15,2)  NULL,
    [income_tax_expense] decimal(15,2)  NULL,
    [net_income] decimal(15,2)  NULL,
    [earnings_per_share] decimal(10,2)  NULL,
    [dividends_per_share] decimal(10,2)  NULL,
    [price_earnings_ratio] decimal(10,2)  NULL,
    [market_capitalization] decimal(15,2)  NULL,
    [shares_outstanding] int  NULL,
    [float_shares] int  NULL,
    [stock_price] decimal(10,2)  NULL,
    [dividend_yield] decimal(10,2)  NULL,
    [dividend_payout_ratio] decimal(10,2)  NULL,
    [book_value_per_share] decimal(10,2)  NULL,
    [price_to_book_ratio] decimal(10,2)  NULL,
    [current_ratio] decimal(10,2)  NULL,
    [quick_ratio] decimal(10,2)  NULL,
    [debt_to_equity_ratio] decimal(10,2)  NULL,
    [created_at] datetime2(7)  NULL,
    [updated_at] datetime2(7)  NULL,
    [industry_sector] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [ceo] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [cfo] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [headquarters_location] nvarchar(255) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [number_of_employees] int  NULL,
    [main_products] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [key_customers] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [competitors] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [mission_statement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [vision_statement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [values_statement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [strategic_plan] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [marketing_strategy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [latest_announcement] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [press_releases] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [annual_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [quarterly_earnings] decimal(10,2)  NULL,
    [half_yearly_earnings] decimal(10,2)  NULL,
    [employee_satisfaction_rate] decimal(5,2)  NULL,
    [customer_satisfaction_rate] decimal(5,2)  NULL,
    [quality_certifications] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [environmental_certifications] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [safety_certifications] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [csr_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [sustainability_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [risk_management_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [corporate_governance_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [audit_committee_report] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [shareholder_meetings] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [board_meetings] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [executive_compensation] decimal(15,2)  NULL,
    [dividend_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [share_repurchase_program] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [related_party_transactions] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [whistleblower_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [antitrust_compliance] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [data_protection_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [privacy_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [intellectual_property_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [competition_law_compliance] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [export_controls_compliance] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [anti_corruption_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [labor_law_compliance] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [tax_compliance] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [health_safety_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [environment_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [supplier_code_of_conduct] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [customer_code_of_conduct] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [employee_code_of_conduct] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [business_continuity_plan] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [disaster_recovery_plan] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [incident_response_plan] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [information_security_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [it_governance_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [cybersecurity_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [records_management_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [retention_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [data_classification_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [acceptable_use_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [remote_work_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [bring_your_own_device_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [password_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [encryption_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [multi_factor_authentication_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL,
    [access_control_policy] nvarchar(max) COLLATE Albanian_100_CI_AI_KS_SC_UTF8  NULL
    )
    GO

ALTER TABLE [dbo].[demo_person_company] SET (LOCK_ESCALATION = TABLE)
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'名字',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'first_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'姓氏',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'last_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'性别',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'gender'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'出生日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'date_of_birth'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'出生地',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'place_of_birth'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'国籍',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'nationality'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'种族',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'ethnicity'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'宗教',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'religion'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'母语',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'mother_tongue'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'身高，单位为米',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'height'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'体重，单位为千克',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'weight'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'眼睛颜色',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'eye_color'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'发色',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'hair_color'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'肤色',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'skin_color'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'血型',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'blood_type'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'家庭住址',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'home_address'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'邮政编码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'postal_code'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'电话号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'phone_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'电子邮箱',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'email'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'社会保障号',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'social_security_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'护照号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'passport_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'护照过期日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'passport_expiration_date'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'驾照号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'drivers_license_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'驾照过期日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'drivers_license_expiration_date'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'银行账号',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'bank_account_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'银行名称',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'bank_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'银行路由号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'bank_routing_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'信用卡号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'credit_card_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'信用卡过期日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'credit_card_expiration_date'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'信用卡CVV码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'credit_card_cvv'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'紧急联系人姓名',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'emergency_contact_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'紧急联系人电话',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'emergency_contact_phone'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'医疗状况',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'medical_conditions'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'过敏情况',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'allergies'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'当前用药',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'current_medications'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'医疗保险号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'medical_insurance_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'医疗保险公司',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'medical_insurance_company'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'教育水平',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'educational_level'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'主修研究领域',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'major_field_of_study'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'颁发学位的机构',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'degree_awarding_institution'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'毕业日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'graduation_date'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'就业状况',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'employment_status'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'雇主名称',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'employer_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'职业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'occupation'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'行业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'industry'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'工作电话号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'work_phone_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'工作电子邮箱',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'work_email'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'工作邮寄地址',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'work_postal_address'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'月收入',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'monthly_income'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'婚姻状况',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'marital_status'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'配偶姓名',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'spouse_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'配偶职业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'spouse_occupation'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'孩子姓名',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'children_names'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'孩子出生日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'children_birthdates'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'孩子性别',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'children_genders'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'父母姓名',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'parents_names'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'父母职业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'parents_occupations'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'兄弟姐妹姓名',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'siblings_names'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'兄弟姐妹出生日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'siblings_birthdates'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'兄弟姐妹职业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'siblings_occupations'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'爱好',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'hobbies'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'喜欢的音乐',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_music'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'喜欢的电影',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_movies'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'喜欢的书籍',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_books'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'政治倾向',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'political_affiliation'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'计算机技能',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'computer_skills'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'会说的语言',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'languages_spoken'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'人生目标',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'life_goals'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最大成就',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'biggest_achievement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最大失败',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'biggest_failure'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最大遗憾',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'biggest_regret'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最美好的回忆',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'best_memory'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'个人座右铭',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'personal_motto'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'喜欢的引言',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_quote'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最敬佩的人',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'most_admired_person'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'个人网站',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'personal_website'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'社交媒体账号',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'social_media_profiles'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最近的旅行目的地',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'recent_travel_destinations'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'梦想的旅行目的地',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'dream_travel_destination'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的菜系',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_cuisine'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的饮品',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_drink'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'宠物名字',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'pet_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'宠物类型',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'pet_type'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的运动',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_sport'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的球队',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_team'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的球员',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_player'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最近读的一本书',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'last_book_read'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的艺术家',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_artist'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的专辑',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_album'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最喜欢的歌曲',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'favorite_song'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'擅长的乐器',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'musical_instrument'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'慈善工作',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'charity_work'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'志愿者经历',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'volunteer_experience'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最近的成就',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'latest_achievement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最大的挑战',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'biggest_challenge'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'未来计划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'future_plans'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'公司名称',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'company_name'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'公司代码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'company_code'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'注册号',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'registration_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'成立日期',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'incorporation_date'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'员工数量',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'employee_count'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'平均工资',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'average_salary'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'主营业务',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'main_business'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'行业分类',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'industry_category'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'公司地址',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'company_address'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'公司网址',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'website_url'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'客服电话',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'customer_service_phone'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'传真号码',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'fax_number'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'电子邮箱',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'email_address'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'法定代表人',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'legal_representative'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'注册地址',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'registered_address'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'经营范围',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'business_scope'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'注册资金',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'registered_capital'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'年营收',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'annual_revenue'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'净利润',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'net_profit'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'总资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'total_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'总负债',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'total_liabilities'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股东权益',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'shareholders_equity'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'留存收益',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'retained_earnings'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'流动资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'current_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'固定资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'fixed_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'无形资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'intangible_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'长期债务',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'long_term_debt'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'流动债务',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'current_debt'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'现金及现金等价物',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'cash_and_equivalents'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'应收账款',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'accounts_receivable'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'存货价值',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'inventory_value'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'预付费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'prepaid_expenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'其他流动资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'other_current_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'物业、厂房及设备',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'property_plant_equipment'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'商誉',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'goodwill'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'专利及许可证',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'patents_and_licenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'软件开发成本',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'software_development_costs'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'递延所得税资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'deferred_tax_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'其他非流动资产',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'other_non_current_assets'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'总收入',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'total_revenue'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'销售成本',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'cost_of_goods_sold'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'毛利润',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'gross_profit'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'营业费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'operating_expenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'销售费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'selling_expenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'一般管理费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'general_administrative_expenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'研发费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'research_development_expenses'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'营业收入',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'operating_income'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'利息费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'interest_expense'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'所得税费用',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'income_tax_expense'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'净收入',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'net_income'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'每股收益',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'earnings_per_share'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'每股股息',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'dividends_per_share'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'市盈率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'price_earnings_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'市值',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'market_capitalization'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'已发行股票',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'shares_outstanding'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'流通股',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'float_shares'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股票价格',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'stock_price'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股息率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'dividend_yield'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股息支付比率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'dividend_payout_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'每股账面价值',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'book_value_per_share'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'市净率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'price_to_book_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'流动比率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'current_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'速动比率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'quick_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'负债权益比率',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'debt_to_equity_ratio'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'记录创建时间',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'created_at'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'记录更新时间',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'updated_at'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'所属行业',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'industry_sector'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'首席执行官',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'ceo'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'首席财务官',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'cfo'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'总部地点',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'headquarters_location'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'员工数量',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'number_of_employees'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'主要产品',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'main_products'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'主要客户',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'key_customers'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'竞争对手',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'competitors'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'使命宣言',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'mission_statement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'愿景宣言',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'vision_statement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'价值观宣言',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'values_statement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'战略规划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'strategic_plan'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'市场策略',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'marketing_strategy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'最新公告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'latest_announcement'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'新闻稿',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'press_releases'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'年报',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'annual_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'季度收益',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'quarterly_earnings'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'半年收益',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'half_yearly_earnings'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'员工满意度',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'employee_satisfaction_rate'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'客户满意度',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'customer_satisfaction_rate'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'质量认证',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'quality_certifications'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'环境认证',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'environmental_certifications'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'安全认证',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'safety_certifications'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'企业社会责任报告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'csr_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'可持续性发展报告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'sustainability_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'风险管理报告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'risk_management_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'公司治理报告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'corporate_governance_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'审计委员会报告',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'audit_committee_report'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股东大会纪要',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'shareholder_meetings'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'董事会会议纪要',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'board_meetings'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'高管薪酬',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'executive_compensation'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股息政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'dividend_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'股份回购计划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'share_repurchase_program'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'关联交易',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'related_party_transactions'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'举报政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'whistleblower_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'反垄断合规',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'antitrust_compliance'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'数据保护政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'data_protection_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'隐私政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'privacy_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'知识产权政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'intellectual_property_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'竞争法合规',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'competition_law_compliance'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'出口管制合规',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'export_controls_compliance'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'反腐败政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'anti_corruption_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'劳动法合规',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'labor_law_compliance'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'税务合规',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'tax_compliance'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'健康与安全政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'health_safety_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'环境政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'environment_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'供应商行为准则',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'supplier_code_of_conduct'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'客户行为准则',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'customer_code_of_conduct'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'员工行为准则',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'employee_code_of_conduct'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'业务连续性计划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'business_continuity_plan'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'灾难恢复计划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'disaster_recovery_plan'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'事件响应计划',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'incident_response_plan'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'信息安全政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'information_security_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'IT治理政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'it_governance_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'网络安全政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'cybersecurity_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'记录管理政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'records_management_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'保留政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'retention_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'数据分类政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'data_classification_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'可接受使用政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'acceptable_use_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'远程工作政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'remote_work_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'自带设备政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'bring_your_own_device_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'密码政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'password_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'加密政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'encryption_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'多因素认证政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'multi_factor_authentication_policy'
    GO

    EXEC sp_addextendedproperty
    'MS_Description', N'访问控制政策',
    'SCHEMA', N'dbo',
    'TABLE', N'demo_person_company',
    'COLUMN', N'access_control_policy'
    GO


-- ----------------------------
-- Primary Key structure for table demo_person_company
-- ----------------------------
ALTER TABLE [dbo].[demo_person_company] ADD CONSTRAINT [PK__demo_person_company] PRIMARY KEY CLUSTERED ([id])
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
    GO