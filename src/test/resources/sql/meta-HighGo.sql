--查表清单
SELECT c.relname as tbl_name,
       cast(obj_description(c.relfilenode,'pg_class') as varchar) as tbl_comment,
       b.schemaname as db_name
FROM pg_class as c,
     pg_tables as b
WHERE
        c.relname = b.tablename
  AND b.schemaname='mysql';

--字段明细
select
    col.table_name as tbl_name,
    '' as tbl_comment,
    col.ordinal_position as col_index,
    col.column_name as col_name,
    d.description as col_comment,
    col.udt_name as data_type ,
    col.character_maximum_length as data_length,
    col.numeric_scale as num_scale,
    tc.constraint_type as is_primary_key,
    col.is_nullable as is_nullable,
    col.column_default as default_value
from
    information_schema.columns col
        join pg_class c on c.relname = col.table_name
        left join pg_description d on d.objoid = c.oid and d.objsubid = col.ordinal_position
        left join information_schema.key_column_usage tku on tku.table_name = col.table_name and tku.column_name = col.column_name
        left join information_schema.table_constraints tc on tc.constraint_name = tku.constraint_name
where
        1 = 1
  and col.table_schema = 'mysql'
  and upper(col.table_name)='SIMS_STUDENT'
order by
    col.ordinal_position asc