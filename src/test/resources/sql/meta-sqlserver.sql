select top 10000
	ROW_NUMBER() OVER (ORDER BY t.table_name) AS tbl_order,
        t.table_name AS tbl_name,
       CONVERT(NVARCHAR(1000),isnull(c.[value],'')) AS tbl_comment,
       t.table_catalog as db_name
from INFORMATION_SCHEMA.tables t
         left join sys.extended_properties c
                   on OBJECT_ID(t.table_name)= c.major_id
                       AND c.minor_id = 0
                       AND class_desc = 'OBJECT_OR_COLUMN'
                       AND name = 'MS_Description'
where '1' <> 'abc';


SELECT
        tbl_name = d.name,
        tbl_comment = '',
        col_index = a.colorder,
        col_name = a.name,
        col_comment = CONVERT(nvarchar(1000),ISNULL(g.[value],'')),
        data_type = b.name,
        num_precision =
        CASE
            WHEN COLUMNPROPERTY(a.id, a.name, 'PRECISION' ) <= 0 THEN NULL ELSE COLUMNPROPERTY(a.id, a.name, 'PRECISION' )
            END,
        num_scale =
        CASE
            WHEN ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 ) <= 0 THEN NULL ELSE ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 )
            END,
        is_nullable =
        CASE
            WHEN a.isnullable = 1 THEN 'Y' ELSE ''
            END,
        is_primary_key = (
            SELECT
                'Y'
            FROM
                information_schema.table_constraints AS tc,
                information_schema.key_column_usage AS kcu
            WHERE tc.constraint_name = kcu.constraint_name
              AND tc.constraint_type = 'PRIMARY KEY'
              AND tc.table_name = d.name
              AND kcu.column_name=a.name
        ),
        default_value = ISNULL(e.text, '')
FROM
    syscolumns a
        INNER JOIN sysobjects d ON a.id= d.id AND d.xtype= 'U' AND d.name<> 'dtproperties'
        LEFT JOIN systypes b ON a.xusertype= b.xusertype
        LEFT JOIN syscomments e ON a.cdefault= e.id
        LEFT JOIN sys.extended_properties g ON a.id= g.major_id AND a.colid= g.minor_id
WHERE '1' <> '' AND upper(d.name)=upper('demo_person_company_00001')
ORDER BY
    a.id,
    a.colorder