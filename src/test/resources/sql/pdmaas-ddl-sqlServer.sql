IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_code_generator]') AND type in (N'U')) DROP TABLE [dbo].[core_code_generator];
CREATE TABLE [dbo].[core_code_generator](
    SERIAL_ID VARCHAR(36) NOT NULL,
    GENERATOR_ID VARCHAR(255) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    CONTENT_ JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '代码生成器', 'SCHEMA', dbo, 'table', core_code_generator, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_code_generator, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '生成器ID', 'SCHEMA', dbo, 'table', core_code_generator, 'column', GENERATOR_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_code_generator, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '生成器类型', 'SCHEMA', dbo, 'table', core_code_generator, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '生成器内容', 'SCHEMA', dbo, 'table', core_code_generator, 'column', CONTENT_;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_code_generator, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_code_generator, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_code_generator, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_code_generator, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_code_generator, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_comparison_data]') AND type in (N'U')) DROP TABLE [dbo].[core_comparison_data];
CREATE TABLE [dbo].[core_comparison_data](
    SERIAL_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36),
    NAME VARCHAR(90),
    INTRO VARCHAR(900),
    DATA_SOURCE_TYPE VARCHAR(32),
    SOURCE_URL VARCHAR(255),
    JSON_TEXT JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '项目比对数据源', 'SCHEMA', dbo, 'table', core_comparison_data, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '名称', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', NAME;
EXEC sp_addextendedproperty 'MS_Description', '项目说明', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '数据源类型', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', DATA_SOURCE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '数据提取URL', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', SOURCE_URL;
EXEC sp_addextendedproperty 'MS_Description', 'json内容', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', JSON_TEXT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_comparison_data, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_data_type]') AND type in (N'U')) DROP TABLE [dbo].[core_data_type];
CREATE TABLE [dbo].[core_data_type](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DATA_TYPE_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(255),
    TYPE_MAPPING JSON,
    OFFICIAL_LOCK VARCHAR(1),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SORT_NO INT,
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据类型', 'SCHEMA', dbo, 'table', core_data_type, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_data_type, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据类型ID', 'SCHEMA', dbo, 'table', core_data_type, 'column', DATA_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_data_type, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '类型代码', 'SCHEMA', dbo, 'table', core_data_type, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '类型名称', 'SCHEMA', dbo, 'table', core_data_type, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '类型映射内容;类型与数据库，代码生成器之间的依赖', 'SCHEMA', dbo, 'table', core_data_type, 'column', TYPE_MAPPING;
EXEC sp_addextendedproperty 'MS_Description', '是否为官方锁定;官方类型，官方更新后，可以联动官方更正', 'SCHEMA', dbo, 'table', core_data_type, 'column', OFFICIAL_LOCK;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_data_type, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_data_type, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_data_type, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_data_type, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_data_type, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', core_data_type, 'column', SORT_NO;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_db_conn]') AND type in (N'U')) DROP TABLE [dbo].[core_db_conn];
CREATE TABLE [dbo].[core_db_conn](
    SERIAL_ID VARCHAR(36) NOT NULL,
    CONN_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    DB_TYPE_ID VARCHAR(36),
    PROPERTIES JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据库连接', 'SCHEMA', dbo, 'table', core_db_conn, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_db_conn, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '连接ID', 'SCHEMA', dbo, 'table', core_db_conn, 'column', CONN_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_db_conn, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '连接代码', 'SCHEMA', dbo, 'table', core_db_conn, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '连接名称', 'SCHEMA', dbo, 'table', core_db_conn, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '数据库连接类型', 'SCHEMA', dbo, 'table', core_db_conn, 'column', DB_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '连接属性', 'SCHEMA', dbo, 'table', core_db_conn, 'column', PROPERTIES;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_db_conn, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_db_conn, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_db_conn, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_db_conn, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_db_conn, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_db_type]') AND type in (N'U')) DROP TABLE [dbo].[core_db_type];
CREATE TABLE [dbo].[core_db_type](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DB_TYPE_ID VARCHAR(255) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DDL_TEMPLATE JSON,
    VERSION_DDL_TEMPLATE JSON,
    OFFICIAL_LOCK VARCHAR(1),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据库模板', 'SCHEMA', dbo, 'table', core_db_type, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_db_type, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据库模板ID', 'SCHEMA', dbo, 'table', core_db_type, 'column', DB_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_db_type, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据库代码', 'SCHEMA', dbo, 'table', core_db_type, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '常规模板', 'SCHEMA', dbo, 'table', core_db_type, 'column', DDL_TEMPLATE;
EXEC sp_addextendedproperty 'MS_Description', '版本模板', 'SCHEMA', dbo, 'table', core_db_type, 'column', VERSION_DDL_TEMPLATE;
EXEC sp_addextendedproperty 'MS_Description', '是否为官方锁定;官方类型，官方更新后，可以联动官方更正', 'SCHEMA', dbo, 'table', core_db_type, 'column', OFFICIAL_LOCK;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_db_type, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_db_type, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_db_type, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_db_type, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_db_type, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_diagram]') AND type in (N'U')) DROP TABLE [dbo].[core_diagram];
CREATE TABLE [dbo].[core_diagram](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DIAGRAM_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    CANVAS_DATA JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SORT_NO INT,
    REMARK VARCHAR(1000),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '关系图', 'SCHEMA', dbo, 'table', core_diagram, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_diagram, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '关系图ID', 'SCHEMA', dbo, 'table', core_diagram, 'column', DIAGRAM_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_diagram, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体代码', 'SCHEMA', dbo, 'table', core_diagram, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '实体名称', 'SCHEMA', dbo, 'table', core_diagram, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '画布数据', 'SCHEMA', dbo, 'table', core_diagram, 'column', CANVAS_DATA;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_diagram, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_diagram, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_diagram, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_diagram, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_diagram, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '排序号', 'SCHEMA', dbo, 'table', core_diagram, 'column', SORT_NO;
EXEC sp_addextendedproperty 'MS_Description', '备注', 'SCHEMA', dbo, 'table', core_diagram, 'column', REMARK;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_dict]') AND type in (N'U')) DROP TABLE [dbo].[core_dict];
CREATE TABLE [dbo].[core_dict](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DICT_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    INTRO VARCHAR(900),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SORT_NO INT,
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据字典', 'SCHEMA', dbo, 'table', core_dict, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_dict, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '字典ID', 'SCHEMA', dbo, 'table', core_dict, 'column', DICT_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_dict, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '字典代码', 'SCHEMA', dbo, 'table', core_dict, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '字典名称', 'SCHEMA', dbo, 'table', core_dict, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '字典说明', 'SCHEMA', dbo, 'table', core_dict, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_dict, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_dict, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_dict, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_dict, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_dict, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '排序号', 'SCHEMA', dbo, 'table', core_dict, 'column', SORT_NO;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_dict_item]') AND type in (N'U')) DROP TABLE [dbo].[core_dict_item];
CREATE TABLE [dbo].[core_dict_item](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DICT_ITEM_ID VARCHAR(36) NOT NULL,
    DICT_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    INTRO VARCHAR(900),
    PARENT_KEY VARCHAR(255),
    ENABLED VARCHAR(1),
    SORT VARCHAR(255),
    ATTR1 VARCHAR(255),
    ATTR2 VARCHAR(255),
    ATTR3 VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据字典项', 'SCHEMA', dbo, 'table', core_dict_item, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_dict_item, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '字典项ID', 'SCHEMA', dbo, 'table', core_dict_item, 'column', DICT_ITEM_ID;
EXEC sp_addextendedproperty 'MS_Description', '字典ID', 'SCHEMA', dbo, 'table', core_dict_item, 'column', DICT_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_dict_item, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '字典项代码', 'SCHEMA', dbo, 'table', core_dict_item, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '字典项名称', 'SCHEMA', dbo, 'table', core_dict_item, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '字典项说明', 'SCHEMA', dbo, 'table', core_dict_item, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '父字典项代码', 'SCHEMA', dbo, 'table', core_dict_item, 'column', PARENT_KEY;
EXEC sp_addextendedproperty 'MS_Description', '是否启用', 'SCHEMA', dbo, 'table', core_dict_item, 'column', ENABLED;
EXEC sp_addextendedproperty 'MS_Description', '排序顺序号', 'SCHEMA', dbo, 'table', core_dict_item, 'column', SORT;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性1', 'SCHEMA', dbo, 'table', core_dict_item, 'column', ATTR1;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性2', 'SCHEMA', dbo, 'table', core_dict_item, 'column', ATTR2;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性3', 'SCHEMA', dbo, 'table', core_dict_item, 'column', ATTR3;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_dict_item, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_dict_item, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_dict_item, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_dict_item, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_dict_item, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_domain]') AND type in (N'U')) DROP TABLE [dbo].[core_domain];
CREATE TABLE [dbo].[core_domain](
    SERIAL_ID VARCHAR(36) NOT NULL,
    DOMAIN_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DATA_TYPE_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(255),
    LEN_ VARCHAR(255),
    SCALE_ VARCHAR(255),
    UI_HINT VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SORT_NO INT,
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据域', 'SCHEMA', dbo, 'table', core_domain, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_domain, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据域ID', 'SCHEMA', dbo, 'table', core_domain, 'column', DOMAIN_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_domain, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据类型ID', 'SCHEMA', dbo, 'table', core_domain, 'column', DATA_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '数据域代码', 'SCHEMA', dbo, 'table', core_domain, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '数据域代码名称', 'SCHEMA', dbo, 'table', core_domain, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '长度', 'SCHEMA', dbo, 'table', core_domain, 'column', LEN_;
EXEC sp_addextendedproperty 'MS_Description', '小数位', 'SCHEMA', dbo, 'table', core_domain, 'column', SCALE_;
EXEC sp_addextendedproperty 'MS_Description', 'UI建议', 'SCHEMA', dbo, 'table', core_domain, 'column', UI_HINT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_domain, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_domain, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_domain, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_domain, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_domain, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', core_domain, 'column', SORT_NO;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_entity]') AND type in (N'U')) DROP TABLE [dbo].[core_entity];
CREATE TABLE [dbo].[core_entity](
    SERIAL_ID VARCHAR(36) NOT NULL,
    ENTITY_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(255),
    ENTITY_TYPE VARCHAR(255),
    COMMENT_ VARCHAR(900),
    PROPERTIES JSON,
    CORRELATIONS JSON,
    REF_ENTITIES VARCHAR(255),
    HEADERS JSON,
    ENV JSON,
    SORT_NO INT,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    NOTES JSON,
    NAME_TEMPLATE VARCHAR(90),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '实体/视图', 'SCHEMA', dbo, 'table', core_entity, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_entity, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体ID', 'SCHEMA', dbo, 'table', core_entity, 'column', ENTITY_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_entity, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体代码', 'SCHEMA', dbo, 'table', core_entity, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '实体名称', 'SCHEMA', dbo, 'table', core_entity, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '实体类型', 'SCHEMA', dbo, 'table', core_entity, 'column', ENTITY_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '实体注释', 'SCHEMA', dbo, 'table', core_entity, 'column', COMMENT_;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性', 'SCHEMA', dbo, 'table', core_entity, 'column', PROPERTIES;
EXEC sp_addextendedproperty 'MS_Description', '关联对象', 'SCHEMA', dbo, 'table', core_entity, 'column', CORRELATIONS;
EXEC sp_addextendedproperty 'MS_Description', '引用实体ID', 'SCHEMA', dbo, 'table', core_entity, 'column', REF_ENTITIES;
EXEC sp_addextendedproperty 'MS_Description', '实体表头展示设置', 'SCHEMA', dbo, 'table', core_entity, 'column', HEADERS;
EXEC sp_addextendedproperty 'MS_Description', '环境信息', 'SCHEMA', dbo, 'table', core_entity, 'column', ENV;
EXEC sp_addextendedproperty 'MS_Description', '排序', 'SCHEMA', dbo, 'table', core_entity, 'column', SORT_NO;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_entity, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_entity, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_entity, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_entity, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_entity, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '批注', 'SCHEMA', dbo, 'table', core_entity, 'column', NOTES;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', core_entity, 'column', NAME_TEMPLATE;


CREATE UNIQUE INDEX core_entity_pk ON core_entity(PROJECT_ID,ENTITY_ID);

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_entity_field]') AND type in (N'U')) DROP TABLE [dbo].[core_entity_field];
CREATE TABLE [dbo].[core_entity_field](
    SERIAL_ID VARCHAR(36) NOT NULL,
    FIELD_ID VARCHAR(36) NOT NULL,
    ENTITY_SERIAL_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(900),
    COMMENT_ VARCHAR(900),
    DATA_TYPE_ID VARCHAR(36),
    LEN_ INT,
    SCALE_ INT,
    PRIMARY_KEY VARCHAR(1),
    NOT_NULL VARCHAR(1),
    AUTO_INCREMENT1 VARCHAR(1),
    DEFAULT_VALUE VARCHAR(255),
    HIDE_IN_GRAPH VARCHAR(1),
    REF_DICT_ID VARCHAR(36),
    DOMAIN_ID VARCHAR(36),
    UI_HINT VARCHAR(36),
    REF_ENTITY VARCHAR(255),
    REF_ENTITY_FIELD VARCHAR(255),
    SORT_NO INT,
    PROPERTIES JSON,
    ATTR9 VARCHAR(90),
    ATTR8 VARCHAR(90),
    ATTR7 VARCHAR(90),
    ATTR6 VARCHAR(90),
    ATTR5 VARCHAR(90),
    ATTR4 VARCHAR(90),
    ATTR3 VARCHAR(90),
    ATTR2 VARCHAR(90),
    ATTR1 VARCHAR(90),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    NOTES JSON,
    REF_STANDARD VARCHAR(36),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '实体字段', 'SCHEMA', dbo, 'table', core_entity_field, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_entity_field, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '字段ID', 'SCHEMA', dbo, 'table', core_entity_field, 'column', FIELD_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体流水号ID', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ENTITY_SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_entity_field, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '字段代码', 'SCHEMA', dbo, 'table', core_entity_field, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '字段名称', 'SCHEMA', dbo, 'table', core_entity_field, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '字段注释', 'SCHEMA', dbo, 'table', core_entity_field, 'column', COMMENT_;
EXEC sp_addextendedproperty 'MS_Description', '数据类型ID', 'SCHEMA', dbo, 'table', core_entity_field, 'column', DATA_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '长度', 'SCHEMA', dbo, 'table', core_entity_field, 'column', LEN_;
EXEC sp_addextendedproperty 'MS_Description', '小数位数', 'SCHEMA', dbo, 'table', core_entity_field, 'column', SCALE_;
EXEC sp_addextendedproperty 'MS_Description', '是否主键', 'SCHEMA', dbo, 'table', core_entity_field, 'column', PRIMARY_KEY;
EXEC sp_addextendedproperty 'MS_Description', '是否允许为空', 'SCHEMA', dbo, 'table', core_entity_field, 'column', NOT_NULL;
EXEC sp_addextendedproperty 'MS_Description', '是否自增', 'SCHEMA', dbo, 'table', core_entity_field, 'column', AUTO_INCREMENT1;
EXEC sp_addextendedproperty 'MS_Description', '默认值', 'SCHEMA', dbo, 'table', core_entity_field, 'column', DEFAULT_VALUE;
EXEC sp_addextendedproperty 'MS_Description', '关系图隐藏', 'SCHEMA', dbo, 'table', core_entity_field, 'column', HIDE_IN_GRAPH;
EXEC sp_addextendedproperty 'MS_Description', '引用数据字典', 'SCHEMA', dbo, 'table', core_entity_field, 'column', REF_DICT_ID;
EXEC sp_addextendedproperty 'MS_Description', '使用的数据域', 'SCHEMA', dbo, 'table', core_entity_field, 'column', DOMAIN_ID;
EXEC sp_addextendedproperty 'MS_Description', 'UI建议设置', 'SCHEMA', dbo, 'table', core_entity_field, 'column', UI_HINT;
EXEC sp_addextendedproperty 'MS_Description', '引用实体', 'SCHEMA', dbo, 'table', core_entity_field, 'column', REF_ENTITY;
EXEC sp_addextendedproperty 'MS_Description', '引用实体字段', 'SCHEMA', dbo, 'table', core_entity_field, 'column', REF_ENTITY_FIELD;
EXEC sp_addextendedproperty 'MS_Description', '排序', 'SCHEMA', dbo, 'table', core_entity_field, 'column', SORT_NO;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性', 'SCHEMA', dbo, 'table', core_entity_field, 'column', PROPERTIES;
EXEC sp_addextendedproperty 'MS_Description', '额外属性9', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR9;
EXEC sp_addextendedproperty 'MS_Description', '额外属性8', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR8;
EXEC sp_addextendedproperty 'MS_Description', '额外属性7', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR7;
EXEC sp_addextendedproperty 'MS_Description', '额外属性6', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR6;
EXEC sp_addextendedproperty 'MS_Description', '额外属性5', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR5;
EXEC sp_addextendedproperty 'MS_Description', '额外属性4', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR4;
EXEC sp_addextendedproperty 'MS_Description', '额外属性3', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR3;
EXEC sp_addextendedproperty 'MS_Description', '额外属性2', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR2;
EXEC sp_addextendedproperty 'MS_Description', '额外属性1', 'SCHEMA', dbo, 'table', core_entity_field, 'column', ATTR1;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_entity_field, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_entity_field, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_entity_field, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_entity_field, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_entity_field, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '批注', 'SCHEMA', dbo, 'table', core_entity_field, 'column', NOTES;
EXEC sp_addextendedproperty 'MS_Description', '标准字段ID', 'SCHEMA', dbo, 'table', core_entity_field, 'column', REF_STANDARD;


CREATE INDEX idx_entity_serial_id ON core_entity_field(ENTITY_SERIAL_ID);
CREATE INDEX cef_project_id ON core_entity_field(PROJECT_ID);

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_entity_index]') AND type in (N'U')) DROP TABLE [dbo].[core_entity_index];
CREATE TABLE [dbo].[core_entity_index](
    SERIAL_ID VARCHAR(36) NOT NULL,
    INDEX_ID VARCHAR(36) NOT NULL,
    ENTITY_SERIAL_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    COMMENT_ VARCHAR(900),
    UNIQUE_ VARCHAR(1),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '实体索引', 'SCHEMA', dbo, 'table', core_entity_index, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_entity_index, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '索引ID', 'SCHEMA', dbo, 'table', core_entity_index, 'column', INDEX_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体流水号ID', 'SCHEMA', dbo, 'table', core_entity_index, 'column', ENTITY_SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_entity_index, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '索引代码', 'SCHEMA', dbo, 'table', core_entity_index, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '索引名称', 'SCHEMA', dbo, 'table', core_entity_index, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '索引注释', 'SCHEMA', dbo, 'table', core_entity_index, 'column', COMMENT_;
EXEC sp_addextendedproperty 'MS_Description', '是否唯一', 'SCHEMA', dbo, 'table', core_entity_index, 'column', UNIQUE_;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_entity_index, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_entity_index, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_entity_index, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_entity_index, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_entity_index, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_entity_index_field]') AND type in (N'U')) DROP TABLE [dbo].[core_entity_index_field];
CREATE TABLE [dbo].[core_entity_index_field](
    SERIAL_ID VARCHAR(36) NOT NULL,
    INDEX_FIELD_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    INDEX_SERIAL_ID VARCHAR(36) NOT NULL,
    FIELD_DEF_KEY VARCHAR(255),
    ASC_OR_DESC VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '实体索引字段', 'SCHEMA', dbo, 'table', core_entity_index_field, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '索引字段ID', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', INDEX_FIELD_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '索引流水ID', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', INDEX_SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '引用字段', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', FIELD_DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '排序方式', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', ASC_OR_DESC;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_entity_index_field, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_project]') AND type in (N'U')) DROP TABLE [dbo].[core_project];
CREATE TABLE [dbo].[core_project](
    PROJECT_ID VARCHAR(36) NOT NULL,
    PROJECT_NAME VARCHAR(90),
    PROJECT_INTRO VARCHAR(900),
    CATALOG_ID VARCHAR(36),
    TOTAL_ENTITIES INT,
    TOTAL_DICTS INT,
    TOTAL_DIAGRAMS INT,
    AVATAR LONGTEXT,
    REVISION VARCHAR(36),
    TENANT_ID VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PROJECT_STATUS VARCHAR(32),
    DEL_USER_ID VARCHAR(36),
    USE_TEMP_ID VARCHAR(36),
    PRIMARY KEY (PROJECT_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '项目', 'SCHEMA', dbo, 'table', core_project, null, null;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_project, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目名', 'SCHEMA', dbo, 'table', core_project, 'column', PROJECT_NAME;
EXEC sp_addextendedproperty 'MS_Description', '项目说明', 'SCHEMA', dbo, 'table', core_project, 'column', PROJECT_INTRO;
EXEC sp_addextendedproperty 'MS_Description', '所在目录', 'SCHEMA', dbo, 'table', core_project, 'column', CATALOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '实体/视图数量', 'SCHEMA', dbo, 'table', core_project, 'column', TOTAL_ENTITIES;
EXEC sp_addextendedproperty 'MS_Description', '数据字典数量', 'SCHEMA', dbo, 'table', core_project, 'column', TOTAL_DICTS;
EXEC sp_addextendedproperty 'MS_Description', '关系图数量', 'SCHEMA', dbo, 'table', core_project, 'column', TOTAL_DIAGRAMS;
EXEC sp_addextendedproperty 'MS_Description', '项目头像', 'SCHEMA', dbo, 'table', core_project, 'column', AVATAR;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_project, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '所属公司ID', 'SCHEMA', dbo, 'table', core_project, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_project, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_project, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_project, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_project, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '项目状态', 'SCHEMA', dbo, 'table', core_project, 'column', PROJECT_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '项目删除者ID', 'SCHEMA', dbo, 'table', core_project, 'column', DEL_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', core_project, 'column', USE_TEMP_ID;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_project_profile]') AND type in (N'U')) DROP TABLE [dbo].[core_project_profile];
CREATE TABLE [dbo].[core_project_profile](
    PROJECT_ID VARCHAR(36) NOT NULL,
    ENTITY_INIT_FIELDS JSON,
    ENTITY_INIT_PROPERTIES JSON,
    DEFAULT_DB_TYPE_ID VARCHAR(255),
    UI_HINTS JSON,
    DATA_TYPE_SUPPORTS JSON,
    CODE_TEMPLATES JSON,
    DOC_TEMPLATE VARCHAR(255),
    RELATION_FIELD_SIZE INT,
    ENTITY_NAME_TEMPLATE JSON,
    ENTITY_HEADERS JSON,
    SQL_DELIMITER VARCHAR(255) DEFAULT  ';',
    EXT_PROPS JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    MODEL_TYPE VARCHAR(255),
    RECENT_COLORS JSON,
    META_DATA JSON,
    PRIMARY KEY (PROJECT_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '项目设置', 'SCHEMA', dbo, 'table', core_project_profile, null, null;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_project_profile, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '新建表默认字段', 'SCHEMA', dbo, 'table', core_project_profile, 'column', ENTITY_INIT_FIELDS;
EXEC sp_addextendedproperty 'MS_Description', '新建表默认属性', 'SCHEMA', dbo, 'table', core_project_profile, 'column', ENTITY_INIT_PROPERTIES;
EXEC sp_addextendedproperty 'MS_Description', '默认数据库类型', 'SCHEMA', dbo, 'table', core_project_profile, 'column', DEFAULT_DB_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', 'UI建议选项', 'SCHEMA', dbo, 'table', core_project_profile, 'column', UI_HINTS;
EXEC sp_addextendedproperty 'MS_Description', '数据类型映射', 'SCHEMA', dbo, 'table', core_project_profile, 'column', DATA_TYPE_SUPPORTS;
EXEC sp_addextendedproperty 'MS_Description', '代码模板', 'SCHEMA', dbo, 'table', core_project_profile, 'column', CODE_TEMPLATES;
EXEC sp_addextendedproperty 'MS_Description', 'WORD文档模板', 'SCHEMA', dbo, 'table', core_project_profile, 'column', DOC_TEMPLATE;
EXEC sp_addextendedproperty 'MS_Description', '关系最大字段数', 'SCHEMA', dbo, 'table', core_project_profile, 'column', RELATION_FIELD_SIZE;
EXEC sp_addextendedproperty 'MS_Description', '实体名称模板', 'SCHEMA', dbo, 'table', core_project_profile, 'column', ENTITY_NAME_TEMPLATE;
EXEC sp_addextendedproperty 'MS_Description', '实体表头展示设置', 'SCHEMA', dbo, 'table', core_project_profile, 'column', ENTITY_HEADERS;
EXEC sp_addextendedproperty 'MS_Description', 'sql分隔符', 'SCHEMA', dbo, 'table', core_project_profile, 'column', SQL_DELIMITER;
EXEC sp_addextendedproperty 'MS_Description', '新增字段默认扩展属性', 'SCHEMA', dbo, 'table', core_project_profile, 'column', EXT_PROPS;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_project_profile, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_project_profile, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_project_profile, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_project_profile, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_project_profile, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '模式类型', 'SCHEMA', dbo, 'table', core_project_profile, 'column', MODEL_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '颜色', 'SCHEMA', dbo, 'table', core_project_profile, 'column', RECENT_COLORS;
EXEC sp_addextendedproperty 'MS_Description', '比对数据源', 'SCHEMA', dbo, 'table', core_project_profile, 'column', META_DATA;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_project_user_hist]') AND type in (N'U')) DROP TABLE [dbo].[core_project_user_hist];
CREATE TABLE [dbo].[core_project_user_hist](
    HIST_ID VARCHAR(255),
    PROJECT_ID VARCHAR(36) NOT NULL,
    HISTORY_CONTENT JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19)
);

EXEC sp_addextendedproperty 'MS_Description', '用户更改历史', 'SCHEMA', dbo, 'table', core_project_user_hist, null, null;
EXEC sp_addextendedproperty 'MS_Description', '历史记录ID', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', HIST_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '历史版本全量内容', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', HISTORY_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_project_user_hist, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_project_version]') AND type in (N'U')) DROP TABLE [dbo].[core_project_version];
CREATE TABLE [dbo].[core_project_version](
    VERSION_ID VARCHAR(36) NOT NULL,
    VERSION_CODE VARCHAR(255),
    VERSION_COMMENT VARCHAR(9000),
    PROJECT_ID VARCHAR(36) NOT NULL,
    HISTORY_CONTENT JSON,
    DIFF_CONTENT JSON,
    VERSION_DDL MEDIUMTEXT,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (VERSION_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '项目版本', 'SCHEMA', dbo, 'table', core_project_version, null, null;
EXEC sp_addextendedproperty 'MS_Description', '历史记录ID', 'SCHEMA', dbo, 'table', core_project_version, 'column', VERSION_ID;
EXEC sp_addextendedproperty 'MS_Description', '版本代码', 'SCHEMA', dbo, 'table', core_project_version, 'column', VERSION_CODE;
EXEC sp_addextendedproperty 'MS_Description', '版本说明', 'SCHEMA', dbo, 'table', core_project_version, 'column', VERSION_COMMENT;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_project_version, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '历史版本全量内容', 'SCHEMA', dbo, 'table', core_project_version, 'column', HISTORY_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '相对上一版本差异内容', 'SCHEMA', dbo, 'table', core_project_version, 'column', DIFF_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '当前版本较前一版DDL', 'SCHEMA', dbo, 'table', core_project_version, 'column', VERSION_DDL;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_project_version, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_project_version, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_project_version, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_project_version, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_project_version, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_referen_catalog]') AND type in (N'U')) DROP TABLE [dbo].[core_referen_catalog];
CREATE TABLE [dbo].[core_referen_catalog](
    SERIAL_ID VARCHAR(36) NOT NULL,
    CATALOG_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '字段库分类', 'SCHEMA', dbo, 'table', core_referen_catalog, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '分类ID', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', CATALOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '分类代码', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '分类名称', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_referen_catalog, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_referen_field]') AND type in (N'U')) DROP TABLE [dbo].[core_referen_field];
CREATE TABLE [dbo].[core_referen_field](
    SERIAL_ID VARCHAR(36) NOT NULL,
    REFEREN_FIELD_ID VARCHAR(36) NOT NULL,
    CATALOG_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    COMMENT_ VARCHAR(900),
    DATA_TYPE_ID VARCHAR(36),
    LEN_ INT,
    SCALE_ INT,
    PRIMARY_KEY VARCHAR(1),
    NOT_NULL VARCHAR(1),
    AUTO_INCREMENT1 VARCHAR(1),
    DEFAULT_VALUE VARCHAR(255),
    HIDE_IN_GRAPH VARCHAR(1),
    REF_DICT_ID VARCHAR(36),
    DOMAIN_ID VARCHAR(36),
    UI_HINT JSON,
    PROPERTIES JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    NOTES JSON,
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '模板库字段', 'SCHEMA', dbo, 'table', core_referen_field, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_referen_field, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '模板库字段ID', 'SCHEMA', dbo, 'table', core_referen_field, 'column', REFEREN_FIELD_ID;
EXEC sp_addextendedproperty 'MS_Description', '分类ID', 'SCHEMA', dbo, 'table', core_referen_field, 'column', CATALOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_referen_field, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '字段代码', 'SCHEMA', dbo, 'table', core_referen_field, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '字段名称', 'SCHEMA', dbo, 'table', core_referen_field, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '字段注释', 'SCHEMA', dbo, 'table', core_referen_field, 'column', COMMENT_;
EXEC sp_addextendedproperty 'MS_Description', '数据类型ID', 'SCHEMA', dbo, 'table', core_referen_field, 'column', DATA_TYPE_ID;
EXEC sp_addextendedproperty 'MS_Description', '长度', 'SCHEMA', dbo, 'table', core_referen_field, 'column', LEN_;
EXEC sp_addextendedproperty 'MS_Description', '小数位数', 'SCHEMA', dbo, 'table', core_referen_field, 'column', SCALE_;
EXEC sp_addextendedproperty 'MS_Description', '是否主键', 'SCHEMA', dbo, 'table', core_referen_field, 'column', PRIMARY_KEY;
EXEC sp_addextendedproperty 'MS_Description', '是否允许为空', 'SCHEMA', dbo, 'table', core_referen_field, 'column', NOT_NULL;
EXEC sp_addextendedproperty 'MS_Description', '是否自增', 'SCHEMA', dbo, 'table', core_referen_field, 'column', AUTO_INCREMENT1;
EXEC sp_addextendedproperty 'MS_Description', '默认值', 'SCHEMA', dbo, 'table', core_referen_field, 'column', DEFAULT_VALUE;
EXEC sp_addextendedproperty 'MS_Description', '关系图隐藏', 'SCHEMA', dbo, 'table', core_referen_field, 'column', HIDE_IN_GRAPH;
EXEC sp_addextendedproperty 'MS_Description', '引用数据字典', 'SCHEMA', dbo, 'table', core_referen_field, 'column', REF_DICT_ID;
EXEC sp_addextendedproperty 'MS_Description', '使用的数据域', 'SCHEMA', dbo, 'table', core_referen_field, 'column', DOMAIN_ID;
EXEC sp_addextendedproperty 'MS_Description', 'UI建议设置', 'SCHEMA', dbo, 'table', core_referen_field, 'column', UI_HINT;
EXEC sp_addextendedproperty 'MS_Description', '扩展属性', 'SCHEMA', dbo, 'table', core_referen_field, 'column', PROPERTIES;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_referen_field, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_referen_field, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_referen_field, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_referen_field, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_referen_field, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '批注', 'SCHEMA', dbo, 'table', core_referen_field, 'column', NOTES;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[core_topic]') AND type in (N'U')) DROP TABLE [dbo].[core_topic];
CREATE TABLE [dbo].[core_topic](
    SERIAL_ID VARCHAR(36) NOT NULL,
    TOPIC_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    DEF_KEY VARCHAR(255),
    DEF_NAME VARCHAR(90),
    REF_ENTITIES JSON,
    REF_VIEWS JSON,
    REF_DIAGRAMS JSON,
    REF_DICTS JSON,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SORT_NO INT,
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '分类主题', 'SCHEMA', dbo, 'table', core_topic, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', core_topic, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '话题ID', 'SCHEMA', dbo, 'table', core_topic, 'column', TOPIC_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', core_topic, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '话题代码', 'SCHEMA', dbo, 'table', core_topic, 'column', DEF_KEY;
EXEC sp_addextendedproperty 'MS_Description', '话题名称', 'SCHEMA', dbo, 'table', core_topic, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '包含的实体', 'SCHEMA', dbo, 'table', core_topic, 'column', REF_ENTITIES;
EXEC sp_addextendedproperty 'MS_Description', '包含的视图', 'SCHEMA', dbo, 'table', core_topic, 'column', REF_VIEWS;
EXEC sp_addextendedproperty 'MS_Description', '包含的关系图', 'SCHEMA', dbo, 'table', core_topic, 'column', REF_DIAGRAMS;
EXEC sp_addextendedproperty 'MS_Description', '包含的字典', 'SCHEMA', dbo, 'table', core_topic, 'column', REF_DICTS;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', core_topic, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', core_topic, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', core_topic, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', core_topic, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', core_topic, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '排序号', 'SCHEMA', dbo, 'table', core_topic, 'column', SORT_NO;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fswk_dict]') AND type in (N'U')) DROP TABLE [dbo].[fswk_dict];
CREATE TABLE [dbo].[fswk_dict](
    KEY_ VARCHAR(36),
    LABEL VARCHAR(255),
    I18N VARCHAR(255),
    INTRO VARCHAR(255),
    TENANTID VARCHAR(255),
    REVISION INT,
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19)
);

EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', fswk_dict, null, null;
EXEC sp_addextendedproperty 'MS_Description', '字典标识号', 'SCHEMA', dbo, 'table', fswk_dict, 'column', KEY_;
EXEC sp_addextendedproperty 'MS_Description', '字典名称', 'SCHEMA', dbo, 'table', fswk_dict, 'column', LABEL;
EXEC sp_addextendedproperty 'MS_Description', '国际化标识', 'SCHEMA', dbo, 'table', fswk_dict, 'column', I18N;
EXEC sp_addextendedproperty 'MS_Description', '字典介绍', 'SCHEMA', dbo, 'table', fswk_dict, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '租户号', 'SCHEMA', dbo, 'table', fswk_dict, 'column', TENANTID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', fswk_dict, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', fswk_dict, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', fswk_dict, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', fswk_dict, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', fswk_dict, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fswk_dict_item]') AND type in (N'U')) DROP TABLE [dbo].[fswk_dict_item];
CREATE TABLE [dbo].[fswk_dict_item](
    DICT_KEY VARCHAR(255),
    KEY_ VARCHAR(36),
    LABEL VARCHAR(255),
    VALUE VARCHAR(255),
    STATUS_ VARCHAR(255),
    LABEL_PINYIN VARCHAR(255),
    SORT INT,
    HOTSPOT VARCHAR(255),
    CORRELATION VARCHAR(255),
    INTRO VARCHAR(255),
    TENANTID VARCHAR(255),
    REVISION INT,
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19)
);

EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', fswk_dict_item, null, null;
EXEC sp_addextendedproperty 'MS_Description', '字典标识号', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', DICT_KEY;
EXEC sp_addextendedproperty 'MS_Description', '字典项标识号', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', KEY_;
EXEC sp_addextendedproperty 'MS_Description', '字典项名称', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', LABEL;
EXEC sp_addextendedproperty 'MS_Description', '值', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', VALUE;
EXEC sp_addextendedproperty 'MS_Description', '状态', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', STATUS_;
EXEC sp_addextendedproperty 'MS_Description', '字典项名称拼音', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', LABEL_PINYIN;
EXEC sp_addextendedproperty 'MS_Description', '字典介绍', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', SORT;
EXEC sp_addextendedproperty 'MS_Description', '热度', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', HOTSPOT;
EXEC sp_addextendedproperty 'MS_Description', '关联数据', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', CORRELATION;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '租户号', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', TENANTID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', fswk_dict_item, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_auth_permission]') AND type in (N'U')) DROP TABLE [dbo].[maas_auth_permission];
CREATE TABLE [dbo].[maas_auth_permission](
    AUTH_ID VARCHAR(36) NOT NULL,
    URL VARCHAR(255),
    ROLE_TYPE VARCHAR(32),
    ACCESS_RIGHT VARCHAR(32),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (AUTH_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '权限', 'SCHEMA', dbo, 'table', maas_auth_permission, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', AUTH_ID;
EXEC sp_addextendedproperty 'MS_Description', '路径', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', URL;
EXEC sp_addextendedproperty 'MS_Description', '角色类型', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', ROLE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '访问权限', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', ACCESS_RIGHT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_auth_permission, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_catalog]') AND type in (N'U')) DROP TABLE [dbo].[maas_catalog];
CREATE TABLE [dbo].[maas_catalog](
    CATALOG_ID VARCHAR(36) NOT NULL,
    PARENT_ID VARCHAR(36),
    OWNER_USER_ID VARCHAR(36) NOT NULL,
    SORT_NUMBER INT,
    CATALOG_NAME VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    TENANT_ID VARCHAR(36),
    PRIMARY KEY (CATALOG_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '分类目录', 'SCHEMA', dbo, 'table', maas_catalog, null, null;
EXEC sp_addextendedproperty 'MS_Description', '目录ID', 'SCHEMA', dbo, 'table', maas_catalog, 'column', CATALOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '父目录ID', 'SCHEMA', dbo, 'table', maas_catalog, 'column', PARENT_ID;
EXEC sp_addextendedproperty 'MS_Description', '归属用户', 'SCHEMA', dbo, 'table', maas_catalog, 'column', OWNER_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '排序号', 'SCHEMA', dbo, 'table', maas_catalog, 'column', SORT_NUMBER;
EXEC sp_addextendedproperty 'MS_Description', '目录名', 'SCHEMA', dbo, 'table', maas_catalog, 'column', CATALOG_NAME;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_catalog, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_catalog, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_catalog, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_catalog, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_catalog, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '所属公司ID', 'SCHEMA', dbo, 'table', maas_catalog, 'column', TENANT_ID;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_client_registe_info]') AND type in (N'U')) DROP TABLE [dbo].[maas_client_registe_info];
CREATE TABLE [dbo].[maas_client_registe_info](
    GENERATE_KEY VARCHAR(255) NOT NULL,
    LICENSE VARCHAR(600),
    ID INT NOT NULL IDENTITY(1,1),
    TENANT_ID VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (ID)
);

EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_client_registe_info, null, null;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', GENERATE_KEY;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', LICENSE;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', ID;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_client_registe_info, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_coproc]') AND type in (N'U')) DROP TABLE [dbo].[maas_coproc];
CREATE TABLE [dbo].[maas_coproc](
    CORPOC_ID VARCHAR(36) NOT NULL,
    USER_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36) NOT NULL,
    ROLE_TYPE VARCHAR(255),
    INVITE_USER_ID VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(36),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(36),
    PRIMARY KEY (CORPOC_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '协同处理', 'SCHEMA', dbo, 'table', maas_coproc, null, null;
EXEC sp_addextendedproperty 'MS_Description', '协同记录ID', 'SCHEMA', dbo, 'table', maas_coproc, 'column', CORPOC_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_coproc, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', maas_coproc, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '协同角色', 'SCHEMA', dbo, 'table', maas_coproc, 'column', ROLE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '邀请人用户ID', 'SCHEMA', dbo, 'table', maas_coproc, 'column', INVITE_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_coproc, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_coproc, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_coproc, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_coproc, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_coproc, 'column', UPDATED_TIME;


CREATE INDEX mc_project_id ON maas_coproc(PROJECT_ID);

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_db_conn]') AND type in (N'U')) DROP TABLE [dbo].[maas_db_conn];
CREATE TABLE [dbo].[maas_db_conn](
    CONN_ID VARCHAR(36) NOT NULL,
    DRIVER_CLASS_NAME VARCHAR(255),
    URL VARCHAR(255),
    USER_NAME VARCHAR(255),
    PASSWORD VARCHAR(255),
    DEF_NAME VARCHAR(255),
    DB_TYPE VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    TENANT_ID VARCHAR(36),
    PRIMARY KEY (CONN_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '数据库连接', 'SCHEMA', dbo, 'table', maas_db_conn, null, null;
EXEC sp_addextendedproperty 'MS_Description', '数据库ID', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', CONN_ID;
EXEC sp_addextendedproperty 'MS_Description', '驱动', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', DRIVER_CLASS_NAME;
EXEC sp_addextendedproperty 'MS_Description', 'url', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', URL;
EXEC sp_addextendedproperty 'MS_Description', '数据库账户名', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', USER_NAME;
EXEC sp_addextendedproperty 'MS_Description', '数据库密码', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', PASSWORD;
EXEC sp_addextendedproperty 'MS_Description', '连接名称', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', DEF_NAME;
EXEC sp_addextendedproperty 'MS_Description', '数据库连接类型', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', DB_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '公司Id', 'SCHEMA', dbo, 'table', maas_db_conn, 'column', TENANT_ID;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_download]') AND type in (N'U')) DROP TABLE [dbo].[maas_download];
CREATE TABLE [dbo].[maas_download](
    DOWNLOAD_ID VARCHAR(36) NOT NULL,
    USER_ID VARCHAR(36) NOT NULL,
    OBJECT_TYPE VARCHAR(32),
    OBJECT_ID VARCHAR(36),
    INTRO VARCHAR(900),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (DOWNLOAD_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '下载', 'SCHEMA', dbo, 'table', maas_download, null, null;
EXEC sp_addextendedproperty 'MS_Description', '下载ID', 'SCHEMA', dbo, 'table', maas_download, 'column', DOWNLOAD_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_download, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '收载对象类型', 'SCHEMA', dbo, 'table', maas_download, 'column', OBJECT_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '对象ID', 'SCHEMA', dbo, 'table', maas_download, 'column', OBJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '收藏说明', 'SCHEMA', dbo, 'table', maas_download, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_download, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_download, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_download, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_download, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_download, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_favor]') AND type in (N'U')) DROP TABLE [dbo].[maas_favor];
CREATE TABLE [dbo].[maas_favor](
    FAVOR_ID VARCHAR(36) NOT NULL,
    USER_ID VARCHAR(36) NOT NULL,
    OBJECT_TYPE VARCHAR(32),
    OBJECT_ID VARCHAR(36),
    INTRO VARCHAR(900),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (FAVOR_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '收藏', 'SCHEMA', dbo, 'table', maas_favor, null, null;
EXEC sp_addextendedproperty 'MS_Description', '收载ID', 'SCHEMA', dbo, 'table', maas_favor, 'column', FAVOR_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_favor, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '收载对象类型', 'SCHEMA', dbo, 'table', maas_favor, 'column', OBJECT_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '对象ID', 'SCHEMA', dbo, 'table', maas_favor, 'column', OBJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '收藏说明', 'SCHEMA', dbo, 'table', maas_favor, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_favor, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_favor, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_favor, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_favor, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_favor, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_issue]') AND type in (N'U')) DROP TABLE [dbo].[maas_issue];
CREATE TABLE [dbo].[maas_issue](
    PROJECT_ID VARCHAR(36) NOT NULL,
    PROJECT_NAME VARCHAR(90),
    PROJECT_INTRO VARCHAR(900),
    PROJECT_INDUSTRY VARCHAR(32),
    PROJECT_TYPE VARCHAR(32),
    PAY_AMOUNT DECIMAL(24,6),
    THUMBNAIL VARCHAR(900),
    STARS DECIMAL(24,6),
    ISSUE_STATUS VARCHAR(32),
    CATALOG_ID VARCHAR(36),
    USER_ID VARCHAR(36),
    TENANT_ID VARCHAR(36),
    OFF_USER_ID VARCHAR(36),
    OFF_TENANT_ID VARCHAR(36),
    VISIT_COUNT INT,
    FAVOR_COUNT INT,
    COPY_COUNT INT,
    REVISION INT,
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    IS_TOP VARCHAR(1),
    PRIMARY KEY (PROJECT_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '项目广场', 'SCHEMA', dbo, 'table', maas_issue, null, null;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', maas_issue, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目名', 'SCHEMA', dbo, 'table', maas_issue, 'column', PROJECT_NAME;
EXEC sp_addextendedproperty 'MS_Description', '项目说明', 'SCHEMA', dbo, 'table', maas_issue, 'column', PROJECT_INTRO;
EXEC sp_addextendedproperty 'MS_Description', '项目行业类型', 'SCHEMA', dbo, 'table', maas_issue, 'column', PROJECT_INDUSTRY;
EXEC sp_addextendedproperty 'MS_Description', '项目上架类型', 'SCHEMA', dbo, 'table', maas_issue, 'column', PROJECT_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '付费金额', 'SCHEMA', dbo, 'table', maas_issue, 'column', PAY_AMOUNT;
EXEC sp_addextendedproperty 'MS_Description', '缩略图', 'SCHEMA', dbo, 'table', maas_issue, 'column', THUMBNAIL;
EXEC sp_addextendedproperty 'MS_Description', '评分星级', 'SCHEMA', dbo, 'table', maas_issue, 'column', STARS;
EXEC sp_addextendedproperty 'MS_Description', '上架状态', 'SCHEMA', dbo, 'table', maas_issue, 'column', ISSUE_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '所在目录', 'SCHEMA', dbo, 'table', maas_issue, 'column', CATALOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '发布用户', 'SCHEMA', dbo, 'table', maas_issue, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '发布企业', 'SCHEMA', dbo, 'table', maas_issue, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '下架用户', 'SCHEMA', dbo, 'table', maas_issue, 'column', OFF_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '下架企业', 'SCHEMA', dbo, 'table', maas_issue, 'column', OFF_TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '查看次数', 'SCHEMA', dbo, 'table', maas_issue, 'column', VISIT_COUNT;
EXEC sp_addextendedproperty 'MS_Description', '收藏次数', 'SCHEMA', dbo, 'table', maas_issue, 'column', FAVOR_COUNT;
EXEC sp_addextendedproperty 'MS_Description', '获取次数', 'SCHEMA', dbo, 'table', maas_issue, 'column', COPY_COUNT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_issue, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_issue, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_issue, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_issue, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_issue, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '是否置顶', 'SCHEMA', dbo, 'table', maas_issue, 'column', IS_TOP;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_issue_rating]') AND type in (N'U')) DROP TABLE [dbo].[maas_issue_rating];
CREATE TABLE [dbo].[maas_issue_rating](
    RATING_ID VARCHAR(36) NOT NULL,
    PROJECT_ID VARCHAR(36),
    USER_ID VARCHAR(36),
    STARS DECIMAL(24,6),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (RATING_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '用户评分', 'SCHEMA', dbo, 'table', maas_issue_rating, null, null;
EXEC sp_addextendedproperty 'MS_Description', '评分ID', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', RATING_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '评分用户', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '评分星级', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', STARS;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_issue_rating, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_maintenance_log]') AND type in (N'U')) DROP TABLE [dbo].[maas_maintenance_log];
CREATE TABLE [dbo].[maas_maintenance_log](
    LOG_ID VARCHAR(255) NOT NULL,
    MAINTENANCE_STATUS VARCHAR(255),
    NOTICE_TITLE VARCHAR(90),
    NOTICE_TEXT VARCHAR(900),
    VERSION VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (LOG_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '版本维护日志', 'SCHEMA', dbo, 'table', maas_maintenance_log, null, null;
EXEC sp_addextendedproperty 'MS_Description', '日志id', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', LOG_ID;
EXEC sp_addextendedproperty 'MS_Description', '维护状态', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', MAINTENANCE_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '维护公告', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', NOTICE_TITLE;
EXEC sp_addextendedproperty 'MS_Description', '维护内容', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', NOTICE_TEXT;
EXEC sp_addextendedproperty 'MS_Description', '版本', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', VERSION;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_maintenance_log, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_order]') AND type in (N'U')) DROP TABLE [dbo].[maas_order];
CREATE TABLE [dbo].[maas_order](
    ORDER_ID VARCHAR(36) NOT NULL,
    USER_ID VARCHAR(36) NOT NULL,
    TENANT_ID VARCHAR(36),
    CREATE_TIME VARCHAR(19),
    TRANS_AMT DECIMAL(24,6),
    TRANS_CONTENT VARCHAR(900),
    ORDER_YEARS INT,
    ORDER_TYPE VARCHAR(32),
    CUBICLE_IDS VARCHAR(255),
    CUBICLE_NUM INT,
    PAY_CHANNEL VARCHAR(32),
    INVOICE_STATUS VARCHAR(32),
    TRANS_STATUS VARCHAR(32),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    ORDER_MONTHS INT,
    PRIMARY KEY (ORDER_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '订单', 'SCHEMA', dbo, 'table', maas_order, null, null;
EXEC sp_addextendedproperty 'MS_Description', '订单ID', 'SCHEMA', dbo, 'table', maas_order, 'column', ORDER_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_order, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '公司ID', 'SCHEMA', dbo, 'table', maas_order, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '交易时间', 'SCHEMA', dbo, 'table', maas_order, 'column', CREATE_TIME;
EXEC sp_addextendedproperty 'MS_Description', '交易金额', 'SCHEMA', dbo, 'table', maas_order, 'column', TRANS_AMT;
EXEC sp_addextendedproperty 'MS_Description', '交易内容', 'SCHEMA', dbo, 'table', maas_order, 'column', TRANS_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '订单时间', 'SCHEMA', dbo, 'table', maas_order, 'column', ORDER_YEARS;
EXEC sp_addextendedproperty 'MS_Description', '订单类型', 'SCHEMA', dbo, 'table', maas_order, 'column', ORDER_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '席位情况', 'SCHEMA', dbo, 'table', maas_order, 'column', CUBICLE_IDS;
EXEC sp_addextendedproperty 'MS_Description', '席位数量', 'SCHEMA', dbo, 'table', maas_order, 'column', CUBICLE_NUM;
EXEC sp_addextendedproperty 'MS_Description', '支付方式', 'SCHEMA', dbo, 'table', maas_order, 'column', PAY_CHANNEL;
EXEC sp_addextendedproperty 'MS_Description', '开票状态', 'SCHEMA', dbo, 'table', maas_order, 'column', INVOICE_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '交易状态', 'SCHEMA', dbo, 'table', maas_order, 'column', TRANS_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_order, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_order, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_order, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_order, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_order, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '订单时间', 'SCHEMA', dbo, 'table', maas_order, 'column', ORDER_MONTHS;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_oss]') AND type in (N'U')) DROP TABLE [dbo].[maas_oss];
CREATE TABLE [dbo].[maas_oss](
    FILE_ID VARCHAR(36) NOT NULL,
    FILE_NAME VARCHAR(255),
    FILE_PATH VARCHAR(255),
    FILE_SIZE INT,
    FILE_TYPE VARCHAR(32),
    STORE_TYPE VARCHAR(10),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(255),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (FILE_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '对象存储记录', 'SCHEMA', dbo, 'table', maas_oss, null, null;
EXEC sp_addextendedproperty 'MS_Description', '文件ID', 'SCHEMA', dbo, 'table', maas_oss, 'column', FILE_ID;
EXEC sp_addextendedproperty 'MS_Description', '文件名', 'SCHEMA', dbo, 'table', maas_oss, 'column', FILE_NAME;
EXEC sp_addextendedproperty 'MS_Description', '文件路径', 'SCHEMA', dbo, 'table', maas_oss, 'column', FILE_PATH;
EXEC sp_addextendedproperty 'MS_Description', '文件大小', 'SCHEMA', dbo, 'table', maas_oss, 'column', FILE_SIZE;
EXEC sp_addextendedproperty 'MS_Description', '文件类型', 'SCHEMA', dbo, 'table', maas_oss, 'column', FILE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '存储类型', 'SCHEMA', dbo, 'table', maas_oss, 'column', STORE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_oss, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_oss, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_oss, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_oss, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_oss, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_price_system]') AND type in (N'U')) DROP TABLE [dbo].[maas_price_system];
CREATE TABLE [dbo].[maas_price_system](
    PRICE_ID VARCHAR(36) NOT NULL,
    TYPE VARCHAR(32),
    AMOUNT DECIMAL(24,6),
    DISCOUNT DECIMAL(24,6),
    YEARS INT,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    MONTHS INT,
    PRIMARY KEY (PRICE_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '定价系统', 'SCHEMA', dbo, 'table', maas_price_system, null, null;
EXEC sp_addextendedproperty 'MS_Description', '流水号', 'SCHEMA', dbo, 'table', maas_price_system, 'column', PRICE_ID;
EXEC sp_addextendedproperty 'MS_Description', '类型', 'SCHEMA', dbo, 'table', maas_price_system, 'column', TYPE;
EXEC sp_addextendedproperty 'MS_Description', '单价', 'SCHEMA', dbo, 'table', maas_price_system, 'column', AMOUNT;
EXEC sp_addextendedproperty 'MS_Description', '折扣', 'SCHEMA', dbo, 'table', maas_price_system, 'column', DISCOUNT;
EXEC sp_addextendedproperty 'MS_Description', '服务时限', 'SCHEMA', dbo, 'table', maas_price_system, 'column', YEARS;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_price_system, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_price_system, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_price_system, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_price_system, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_price_system, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '服务时限', 'SCHEMA', dbo, 'table', maas_price_system, 'column', MONTHS;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_registe_info]') AND type in (N'U')) DROP TABLE [dbo].[maas_registe_info];
CREATE TABLE [dbo].[maas_registe_info](
    TALNENT_ID VARCHAR(36) NOT NULL,
    LICENSE VARCHAR(600),
    GENERATE_KEY VARCHAR(255),
    TALNENT_NAME VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (TALNENT_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_registe_info, null, null;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', TALNENT_ID;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', LICENSE;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', GENERATE_KEY;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', TALNENT_NAME;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_registe_info, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_tenant]') AND type in (N'U')) DROP TABLE [dbo].[maas_tenant];
CREATE TABLE [dbo].[maas_tenant](
    TENANT_ID VARCHAR(36) NOT NULL,
    TENANT_NAME VARCHAR(90),
    COMPANY_INDUSTRY VARCHAR(32),
    COMPANY_PHONE VARCHAR(255),
    COMPANY_DISTRICT VARCHAR(255),
    COMPANY_ADDRESS VARCHAR(255),
    AVATAR LONGTEXT,
    TENANT_RANK VARCHAR(32),
    TOTAL_SEAT INT DEFAULT  1,
    EXPIRE_DATE VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    USER_ID VARCHAR(36),
    AU_TYPE VARCHAR(32),
    STATUS VARCHAR(32),
    PRIMARY KEY (TENANT_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '租户企业', 'SCHEMA', dbo, 'table', maas_tenant, null, null;
EXEC sp_addextendedproperty 'MS_Description', '公司ID', 'SCHEMA', dbo, 'table', maas_tenant, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '公司名', 'SCHEMA', dbo, 'table', maas_tenant, 'column', TENANT_NAME;
EXEC sp_addextendedproperty 'MS_Description', '公司行业类型', 'SCHEMA', dbo, 'table', maas_tenant, 'column', COMPANY_INDUSTRY;
EXEC sp_addextendedproperty 'MS_Description', '公司联系电话', 'SCHEMA', dbo, 'table', maas_tenant, 'column', COMPANY_PHONE;
EXEC sp_addextendedproperty 'MS_Description', '公司所在区域', 'SCHEMA', dbo, 'table', maas_tenant, 'column', COMPANY_DISTRICT;
EXEC sp_addextendedproperty 'MS_Description', '公司地址', 'SCHEMA', dbo, 'table', maas_tenant, 'column', COMPANY_ADDRESS;
EXEC sp_addextendedproperty 'MS_Description', '公司LOGO', 'SCHEMA', dbo, 'table', maas_tenant, 'column', AVATAR;
EXEC sp_addextendedproperty 'MS_Description', '企业级别', 'SCHEMA', dbo, 'table', maas_tenant, 'column', TENANT_RANK;
EXEC sp_addextendedproperty 'MS_Description', '购买的席位数', 'SCHEMA', dbo, 'table', maas_tenant, 'column', TOTAL_SEAT;
EXEC sp_addextendedproperty 'MS_Description', '到期日', 'SCHEMA', dbo, 'table', maas_tenant, 'column', EXPIRE_DATE;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_tenant, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_tenant, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_tenant, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_tenant, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_tenant, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '企业所有者', 'SCHEMA', dbo, 'table', maas_tenant, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '企业认证类型', 'SCHEMA', dbo, 'table', maas_tenant, 'column', AU_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '审批状态', 'SCHEMA', dbo, 'table', maas_tenant, 'column', STATUS;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_tenant_role]') AND type in (N'U')) DROP TABLE [dbo].[maas_tenant_role];
CREATE TABLE [dbo].[maas_tenant_role](
    TENANT_ROLE_ID VARCHAR(36) NOT NULL,
    TENANT_ID VARCHAR(36) NOT NULL,
    ROLE_TYPE VARCHAR(255) NOT NULL,
    USER_ID VARCHAR(36) NOT NULL,
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (TENANT_ROLE_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '企业角色', 'SCHEMA', dbo, 'table', maas_tenant_role, null, null;
EXEC sp_addextendedproperty 'MS_Description', '企业角色id', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', TENANT_ROLE_ID;
EXEC sp_addextendedproperty 'MS_Description', '公司ID', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '角色类型', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', ROLE_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_tenant_role, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_user]') AND type in (N'U')) DROP TABLE [dbo].[maas_user];
CREATE TABLE [dbo].[maas_user](
    USER_ID VARCHAR(36) NOT NULL,
    USER_NAME VARCHAR(255),
    PASSWORD VARCHAR(255),
    NICK_NAME VARCHAR(90),
    REAL_NAME VARCHAR(255),
    MOBILE_PHONE VARCHAR(255),
    MOBILE_PHONE_BIND VARCHAR(1),
    WECHAT_OPEN_ID VARCHAR(255),
    WECHAT_BIND VARCHAR(1),
    EMAIL VARCHAR(255),
    EMAIL_BIND VARCHAR(1),
    GITEE_ID VARCHAR(255),
    GITEE_BIND VARCHAR(1),
    AVATAR VARCHAR(900),
    USER_RANK VARCHAR(32),
    STATUS VARCHAR(32),
    TENANT_ID VARCHAR(36),
    TENANT_NAME VARCHAR(90),
    COMPANY_INDUSTRY VARCHAR(32),
    USER_WORK_AS VARCHAR(32),
    INTRO VARCHAR(900),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    LAST_LOGIN VARCHAR(19),
    PRIMARY KEY (USER_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '用户信息', 'SCHEMA', dbo, 'table', maas_user, null, null;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_user, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户名', 'SCHEMA', dbo, 'table', maas_user, 'column', USER_NAME;
EXEC sp_addextendedproperty 'MS_Description', '密码', 'SCHEMA', dbo, 'table', maas_user, 'column', PASSWORD;
EXEC sp_addextendedproperty 'MS_Description', '昵称', 'SCHEMA', dbo, 'table', maas_user, 'column', NICK_NAME;
EXEC sp_addextendedproperty 'MS_Description', '姓名', 'SCHEMA', dbo, 'table', maas_user, 'column', REAL_NAME;
EXEC sp_addextendedproperty 'MS_Description', '手机号', 'SCHEMA', dbo, 'table', maas_user, 'column', MOBILE_PHONE;
EXEC sp_addextendedproperty 'MS_Description', '手机号绑定', 'SCHEMA', dbo, 'table', maas_user, 'column', MOBILE_PHONE_BIND;
EXEC sp_addextendedproperty 'MS_Description', '微信号', 'SCHEMA', dbo, 'table', maas_user, 'column', WECHAT_OPEN_ID;
EXEC sp_addextendedproperty 'MS_Description', '微信号绑定', 'SCHEMA', dbo, 'table', maas_user, 'column', WECHAT_BIND;
EXEC sp_addextendedproperty 'MS_Description', '邮件地址', 'SCHEMA', dbo, 'table', maas_user, 'column', EMAIL;
EXEC sp_addextendedproperty 'MS_Description', '邮件绑定', 'SCHEMA', dbo, 'table', maas_user, 'column', EMAIL_BIND;
EXEC sp_addextendedproperty 'MS_Description', 'GITEE账号', 'SCHEMA', dbo, 'table', maas_user, 'column', GITEE_ID;
EXEC sp_addextendedproperty 'MS_Description', 'GITEE绑定', 'SCHEMA', dbo, 'table', maas_user, 'column', GITEE_BIND;
EXEC sp_addextendedproperty 'MS_Description', '头像', 'SCHEMA', dbo, 'table', maas_user, 'column', AVATAR;
EXEC sp_addextendedproperty 'MS_Description', '用户等级', 'SCHEMA', dbo, 'table', maas_user, 'column', USER_RANK;
EXEC sp_addextendedproperty 'MS_Description', '用户状态', 'SCHEMA', dbo, 'table', maas_user, 'column', STATUS;
EXEC sp_addextendedproperty 'MS_Description', '所属公司ID', 'SCHEMA', dbo, 'table', maas_user, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '所属公司名', 'SCHEMA', dbo, 'table', maas_user, 'column', TENANT_NAME;
EXEC sp_addextendedproperty 'MS_Description', '公司行业类型', 'SCHEMA', dbo, 'table', maas_user, 'column', COMPANY_INDUSTRY;
EXEC sp_addextendedproperty 'MS_Description', '用户职位', 'SCHEMA', dbo, 'table', maas_user, 'column', USER_WORK_AS;
EXEC sp_addextendedproperty 'MS_Description', '用户介绍', 'SCHEMA', dbo, 'table', maas_user, 'column', INTRO;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_user, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_user, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_user, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_user, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_user, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '上次登录时间', 'SCHEMA', dbo, 'table', maas_user, 'column', LAST_LOGIN;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[maas_user_log]') AND type in (N'U')) DROP TABLE [dbo].[maas_user_log];
CREATE TABLE [dbo].[maas_user_log](
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    SERIAL_ID VARCHAR(32) NOT NULL,
    USER_ID VARCHAR(32),
    TENANT_ID VARCHAR(32),
    PROJECT_ID VARCHAR(32),
    OPERATON_TYPE VARCHAR(32),
    ALL_CONTENT JSON,
    UPDATE_CONTENT VARCHAR(90),
    PRIMARY KEY (SERIAL_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '用户日志', 'SCHEMA', dbo, 'table', maas_user_log, null, null;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', maas_user_log, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', maas_user_log, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', maas_user_log, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', maas_user_log, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', maas_user_log, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '序列号', 'SCHEMA', dbo, 'table', maas_user_log, 'column', SERIAL_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', maas_user_log, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '企业ID;用户查看那些企业是活跃的', 'SCHEMA', dbo, 'table', maas_user_log, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '项目ID;用于查看那些项目是活跃的', 'SCHEMA', dbo, 'table', maas_user_log, 'column', PROJECT_ID;
EXEC sp_addextendedproperty 'MS_Description', '操作类型', 'SCHEMA', dbo, 'table', maas_user_log, 'column', OPERATON_TYPE;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_user_log, 'column', ALL_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '', 'SCHEMA', dbo, 'table', maas_user_log, 'column', UPDATE_CONTENT;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mass_cubicle]') AND type in (N'U')) DROP TABLE [dbo].[mass_cubicle];
CREATE TABLE [dbo].[mass_cubicle](
    CUBICLE_ID VARCHAR(36) NOT NULL,
    TENANT_ID VARCHAR(36) NOT NULL,
    START_DATE VARCHAR(19),
    EXPIRE_DATE VARCHAR(19),
    CUBICLE_STATUS VARCHAR(32),
    OWNER_USER_ID VARCHAR(36),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    LICENSE_CODE VARCHAR(600),
    PRIMARY KEY (CUBICLE_ID)
);

EXEC sp_addextendedproperty 'MS_Description', '席位', 'SCHEMA', dbo, 'table', mass_cubicle, null, null;
EXEC sp_addextendedproperty 'MS_Description', '席位ID', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', CUBICLE_ID;
EXEC sp_addextendedproperty 'MS_Description', '公司ID', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', TENANT_ID;
EXEC sp_addextendedproperty 'MS_Description', '开始日期', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', START_DATE;
EXEC sp_addextendedproperty 'MS_Description', '结束日期', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', EXPIRE_DATE;
EXEC sp_addextendedproperty 'MS_Description', '席位状态', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', CUBICLE_STATUS;
EXEC sp_addextendedproperty 'MS_Description', '席位用户', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', OWNER_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', UPDATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '席位的license', 'SCHEMA', dbo, 'table', mass_cubicle, 'column', LICENSE_CODE;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mass_order_item]') AND type in (N'U')) DROP TABLE [dbo].[mass_order_item];
CREATE TABLE [dbo].[mass_order_item](
    ORDER_ITEM VARCHAR(36) NOT NULL,
    ORDER_ID VARCHAR(36) NOT NULL,
    CUBICLE_ID VARCHAR(36) NOT NULL,
    ITEM_AMT DECIMAL(24,6),
    ITEM_CONTENT VARCHAR(900),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19),
    PRIMARY KEY (ORDER_ITEM)
);

EXEC sp_addextendedproperty 'MS_Description', '订购席位明细', 'SCHEMA', dbo, 'table', mass_order_item, null, null;
EXEC sp_addextendedproperty 'MS_Description', '订单明细ID', 'SCHEMA', dbo, 'table', mass_order_item, 'column', ORDER_ITEM;
EXEC sp_addextendedproperty 'MS_Description', '订单ID', 'SCHEMA', dbo, 'table', mass_order_item, 'column', ORDER_ID;
EXEC sp_addextendedproperty 'MS_Description', '席位ID', 'SCHEMA', dbo, 'table', mass_order_item, 'column', CUBICLE_ID;
EXEC sp_addextendedproperty 'MS_Description', '明细金额', 'SCHEMA', dbo, 'table', mass_order_item, 'column', ITEM_AMT;
EXEC sp_addextendedproperty 'MS_Description', '明细内容', 'SCHEMA', dbo, 'table', mass_order_item, 'column', ITEM_CONTENT;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', mass_order_item, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', mass_order_item, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', mass_order_item, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', mass_order_item, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', mass_order_item, 'column', UPDATED_TIME;

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mass_recent_invite]') AND type in (N'U')) DROP TABLE [dbo].[mass_recent_invite];
CREATE TABLE [dbo].[mass_recent_invite](
    RECENT_INVITE_ID VARCHAR(36),
    USER_ID VARCHAR(36) NOT NULL,
    INVITE_USER_ID VARCHAR(255),
    REVISION VARCHAR(36),
    CREATED_BY VARCHAR(36),
    CREATED_TIME VARCHAR(19),
    UPDATED_BY VARCHAR(36),
    UPDATED_TIME VARCHAR(19)
);

EXEC sp_addextendedproperty 'MS_Description', '最近邀请的用户', 'SCHEMA', dbo, 'table', mass_recent_invite, null, null;
EXEC sp_addextendedproperty 'MS_Description', '记录流水号', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', RECENT_INVITE_ID;
EXEC sp_addextendedproperty 'MS_Description', '用户ID', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '被邀请用户ID', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', INVITE_USER_ID;
EXEC sp_addextendedproperty 'MS_Description', '乐观锁', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', REVISION;
EXEC sp_addextendedproperty 'MS_Description', '创建人', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', CREATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '创建时间', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', CREATED_TIME;
EXEC sp_addextendedproperty 'MS_Description', '更新人', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', UPDATED_BY;
EXEC sp_addextendedproperty 'MS_Description', '更新时间', 'SCHEMA', dbo, 'table', mass_recent_invite, 'column', UPDATED_TIME;

