DROP TABLE IF EXISTS SIMS_STUDENT;
CREATE TABLE SIMS_STUDENT(
    `COLLEGE_ID` VARCHAR(32) NOT NULL   COMMENT '所在学院ID' ,
    `CLASS_ID` VARCHAR(32) NOT NULL   COMMENT '所在班级ID' ,
    `STUDENT_ID` VARCHAR(32) NOT NULL AUTO_INCREMENT  COMMENT '学生ID' ,
    `STUDENT_NAME` VARCHAR(90)    COMMENT '学生姓名' ,
    `ENG_NAME` VARCHAR(90)    COMMENT '英文名' ,
    `ID_CARD_NO` VARCHAR(255)    COMMENT '身份证号' ,
    `MOBILE_PHONE` VARCHAR(255)    COMMENT '手机号;11位手机号' ,
    `GENDER` VARCHAR(32) NOT NULL  DEFAULT 'M' COMMENT '性别;性别说明' ,
    `MONTHLY_SALARY` DECIMAL(24,6)    COMMENT '月薪' ,
    `BIRTH` DATETIME    COMMENT '出生日期' ,
    `AVATAR` INT    COMMENT '头像' ,
    `HEIGHT` INT    COMMENT '身高' ,
    `WEIGHT` INT    COMMENT '体重' ,
    `NATION` VARCHAR(32)   DEFAULT '01' COMMENT '名族' ,
    `POLITICAL` VARCHAR(32)    COMMENT '政治面貌' ,
    `MARITAL` VARCHAR(32)   DEFAULT 'UNMARRIED' COMMENT '婚姻状况' ,
    `DOMICILE_PLACE_PROVINCE` VARCHAR(255)    COMMENT '籍贯（省）' ,
    `DOMICILE_PLACE_CITY` VARCHAR(255)    COMMENT '籍贯（市）' ,
    `DOMICILE_PLACE_ADDRESS` VARCHAR(255)    COMMENT '户籍地址' ,
    `HOBBY` VARCHAR(255)    COMMENT '爱好' ,
    `INTRO` VARCHAR(900)    COMMENT '简要介绍' ,
    `PRESENT_ADDRESS` VARCHAR(255)    COMMENT '居住地址' ,
    `EMAIL` VARCHAR(255)    COMMENT '电子邮件' ,
    `ENTRY_DATE` DATETIME    COMMENT '入学日期' ,
    `STATUS` VARCHAR(32)   DEFAULT 'Normal' COMMENT '状态' ,
    `TENANT_ID` VARCHAR(32)    COMMENT '租户号' ,
    `REVISION` INT    COMMENT '乐观锁' ,
    `CREATED_BY` VARCHAR(32)    COMMENT '创建人' ,
    `CREATED_TIME` DATETIME   DEFAULT sysdate COMMENT '创建时间' ,
    `UPDATED_BY` VARCHAR(32)    COMMENT '更新人' ,
    `UPDATED_TIME` DATETIME    COMMENT '更新时间' ,
    PRIMARY KEY (STUDENT_ID)
)  COMMENT = '学生';


CREATE INDEX IDX_SMIS_STUDENT_01 ON SIMS_STUDENT(STUDENT_NAME,ENG_NAME);
CREATE INDEX IDX_SMIS_STUDENT_CERT ON SIMS_STUDENT(ID_CARD_NO);

DROP TABLE IF EXISTS SIMS_TEACHER;
CREATE TABLE SIMS_TEACHER(
    `COLLEGE_ID` VARCHAR(32) NOT NULL   COMMENT '所在学院ID' ,
    `TEACHER_ID` VARCHAR(32) NOT NULL   COMMENT '教师ID' ,
    `TEACHER_NAME` VARCHAR(90)    COMMENT '姓名' ,
    `GENDER` VARCHAR(32)   DEFAULT 'M' COMMENT '性别' ,
    `BIRTH` DATETIME    COMMENT '出生日期' ,
    `GRADUATE_INSTITUTION` VARCHAR(90)    COMMENT '毕业院校' ,
    `PRACTICE_YEARS` INT    COMMENT '从业年限' ,
    `POLITICAL` VARCHAR(32)    COMMENT '政治面貌' ,
    `MARITAL` VARCHAR(32)   DEFAULT 'UNMARRIED' COMMENT '婚姻状况' ,
    `AVATAR` VARCHAR(255)    COMMENT '头像' ,
    `INTRO` VARCHAR(900)    COMMENT '介绍' ,
    `TENANT_ID` VARCHAR(32)    COMMENT '租户号' ,
    `REVISION` INT    COMMENT '乐观锁' ,
    `CREATED_BY` VARCHAR(32)    COMMENT '创建人' ,
    `CREATED_TIME` DATETIME    COMMENT '创建时间' ,
    `UPDATED_BY` VARCHAR(32)    COMMENT '更新人' ,
    `UPDATED_TIME` DATETIME    COMMENT '更新时间' ,
    PRIMARY KEY (TEACHER_ID)
)  COMMENT = '教师';

DROP TABLE IF EXISTS SIMS_ADMIN;
CREATE TABLE SIMS_ADMIN(
    `ADMIN_ID` VARCHAR(32) NOT NULL   COMMENT '管理员ID' ,
    `ADMIN_NAME` VARCHAR(90)    COMMENT '管理员用户名' ,
    `ADMIN_PASS` VARCHAR(255)    COMMENT '管理员密码' ,
    `TENANT_ID` VARCHAR(32)    COMMENT '租户号' ,
    `REVISION` VARCHAR(32)    COMMENT '乐观锁' ,
    `CREATED_BY` VARCHAR(32)    COMMENT '创建人' ,
    `CREATED_TIME` DATETIME    COMMENT '创建时间' ,
    `UPDATED_BY` VARCHAR(32)    COMMENT '更新人' ,
    `UPDATED_TIME` DATETIME    COMMENT '更新时间' ,
    PRIMARY KEY (ADMIN_ID)
)  COMMENT = '管理员';

